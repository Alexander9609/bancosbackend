import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IPerfil, Perfil } from 'app/shared/model/perfil.model';
import { PerfilService } from './perfil.service';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-perfil-update',
  templateUrl: './perfil-update.component.html'
})
export class PerfilUpdateComponent implements OnInit {
  perfil: IPerfil;
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    tipoIdentificacion: [],
    identificacion: [],
    edad: [],
    sexo: [],
    direccion: [],
    fechaNacimiento: [],
    nacionalidad: [],
    telefono: [],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected perfilService: PerfilService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ perfil }) => {
      this.updateForm(perfil);
      this.perfil = perfil;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(perfil: IPerfil) {
    this.editForm.patchValue({
      id: perfil.id,
      tipoIdentificacion: perfil.tipoIdentificacion,
      identificacion: perfil.identificacion,
      edad: perfil.edad,
      sexo: perfil.sexo,
      direccion: perfil.direccion,
      fechaNacimiento: perfil.fechaNacimiento != null ? perfil.fechaNacimiento.format(DATE_TIME_FORMAT) : null,
      nacionalidad: perfil.nacionalidad,
      telefono: perfil.telefono,
      user: perfil.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const perfil = this.createFromForm();
    if (perfil.id !== undefined) {
      this.subscribeToSaveResponse(this.perfilService.update(perfil));
    } else {
      this.subscribeToSaveResponse(this.perfilService.create(perfil));
    }
  }

  private createFromForm(): IPerfil {
    const entity = {
      ...new Perfil(),
      id: this.editForm.get(['id']).value,
      tipoIdentificacion: this.editForm.get(['tipoIdentificacion']).value,
      identificacion: this.editForm.get(['identificacion']).value,
      edad: this.editForm.get(['edad']).value,
      sexo: this.editForm.get(['sexo']).value,
      direccion: this.editForm.get(['direccion']).value,
      fechaNacimiento:
        this.editForm.get(['fechaNacimiento']).value != null
          ? moment(this.editForm.get(['fechaNacimiento']).value, DATE_TIME_FORMAT)
          : undefined,
      nacionalidad: this.editForm.get(['nacionalidad']).value,
      telefono: this.editForm.get(['telefono']).value,
      user: this.editForm.get(['user']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPerfil>>) {
    result.subscribe((res: HttpResponse<IPerfil>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
