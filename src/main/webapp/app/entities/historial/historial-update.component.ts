import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IHistorial, Historial } from 'app/shared/model/historial.model';
import { HistorialService } from './historial.service';
import { ICuenta } from 'app/shared/model/cuenta.model';
import { CuentaService } from 'app/entities/cuenta';
import { ICuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';
import { CuentasAsociadasService } from 'app/entities/cuentas-asociadas';

@Component({
  selector: 'jhi-historial-update',
  templateUrl: './historial-update.component.html'
})
export class HistorialUpdateComponent implements OnInit {
  historial: IHistorial;
  isSaving: boolean;

  cuentas: ICuenta[];

  cuentasasociadas: ICuentasAsociadas[];

  editForm = this.fb.group({
    id: [],
    montoHistorial: [],
    descripcionHistorial: [],
    idTransaccion: [],
    fechaHistorial: [],
    cuenta: [],
    cuentasAsociadas: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected historialService: HistorialService,
    protected cuentaService: CuentaService,
    protected cuentasAsociadasService: CuentasAsociadasService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ historial }) => {
      this.updateForm(historial);
      this.historial = historial;
    });
    this.cuentaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICuenta[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICuenta[]>) => response.body)
      )
      .subscribe((res: ICuenta[]) => (this.cuentas = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.cuentasAsociadasService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICuentasAsociadas[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICuentasAsociadas[]>) => response.body)
      )
      .subscribe((res: ICuentasAsociadas[]) => (this.cuentasasociadas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(historial: IHistorial) {
    this.editForm.patchValue({
      id: historial.id,
      montoHistorial: historial.montoHistorial,
      descripcionHistorial: historial.descripcionHistorial,
      idTransaccion: historial.idTransaccion,
      fechaHistorial: historial.fechaHistorial != null ? historial.fechaHistorial.format(DATE_TIME_FORMAT) : null,
      cuenta: historial.cuenta,
      cuentasAsociadas: historial.cuentasAsociadas
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const historial = this.createFromForm();
    if (historial.id !== undefined) {
      this.subscribeToSaveResponse(this.historialService.update(historial));
    } else {
      this.subscribeToSaveResponse(this.historialService.create(historial));
    }
  }

  private createFromForm(): IHistorial {
    const entity = {
      ...new Historial(),
      id: this.editForm.get(['id']).value,
      montoHistorial: this.editForm.get(['montoHistorial']).value,
      descripcionHistorial: this.editForm.get(['descripcionHistorial']).value,
      idTransaccion: this.editForm.get(['idTransaccion']).value,
      fechaHistorial:
        this.editForm.get(['fechaHistorial']).value != null
          ? moment(this.editForm.get(['fechaHistorial']).value, DATE_TIME_FORMAT)
          : undefined,
      cuenta: this.editForm.get(['cuenta']).value,
      cuentasAsociadas: this.editForm.get(['cuentasAsociadas']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHistorial>>) {
    result.subscribe((res: HttpResponse<IHistorial>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCuentaById(index: number, item: ICuenta) {
    return item.id;
  }

  trackCuentasAsociadasById(index: number, item: ICuentasAsociadas) {
    return item.id;
  }
}
