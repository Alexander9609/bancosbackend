import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IHistorial } from 'app/shared/model/historial.model';

type EntityResponseType = HttpResponse<IHistorial>;
type EntityArrayResponseType = HttpResponse<IHistorial[]>;

@Injectable({ providedIn: 'root' })
export class HistorialService {
  public resourceUrl = SERVER_API_URL + 'api/historials';

  constructor(protected http: HttpClient) {}

  create(historial: IHistorial): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(historial);
    return this.http
      .post<IHistorial>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(historial: IHistorial): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(historial);
    return this.http
      .put<IHistorial>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IHistorial>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IHistorial[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(historial: IHistorial): IHistorial {
    const copy: IHistorial = Object.assign({}, historial, {
      fechaHistorial: historial.fechaHistorial != null && historial.fechaHistorial.isValid() ? historial.fechaHistorial.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaHistorial = res.body.fechaHistorial != null ? moment(res.body.fechaHistorial) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((historial: IHistorial) => {
        historial.fechaHistorial = historial.fechaHistorial != null ? moment(historial.fechaHistorial) : null;
      });
    }
    return res;
  }
}
