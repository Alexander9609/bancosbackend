import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IHistorial } from 'app/shared/model/historial.model';
import { HistorialService } from './historial.service';

@Component({
  selector: 'jhi-historial-delete-dialog',
  templateUrl: './historial-delete-dialog.component.html'
})
export class HistorialDeleteDialogComponent {
  historial: IHistorial;

  constructor(protected historialService: HistorialService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.historialService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'historialListModification',
        content: 'Deleted an historial'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-historial-delete-popup',
  template: ''
})
export class HistorialDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ historial }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(HistorialDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.historial = historial;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/historial', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/historial', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
