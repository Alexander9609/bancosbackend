import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Nexos20SharedModule } from 'app/shared';
import {
  HistorialComponent,
  HistorialDetailComponent,
  HistorialUpdateComponent,
  HistorialDeletePopupComponent,
  HistorialDeleteDialogComponent,
  historialRoute,
  historialPopupRoute
} from './';

const ENTITY_STATES = [...historialRoute, ...historialPopupRoute];

@NgModule({
  imports: [Nexos20SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    HistorialComponent,
    HistorialDetailComponent,
    HistorialUpdateComponent,
    HistorialDeleteDialogComponent,
    HistorialDeletePopupComponent
  ],
  entryComponents: [HistorialComponent, HistorialUpdateComponent, HistorialDeleteDialogComponent, HistorialDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Nexos20HistorialModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
