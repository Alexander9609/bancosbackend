import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Historial } from 'app/shared/model/historial.model';
import { HistorialService } from './historial.service';
import { HistorialComponent } from './historial.component';
import { HistorialDetailComponent } from './historial-detail.component';
import { HistorialUpdateComponent } from './historial-update.component';
import { HistorialDeletePopupComponent } from './historial-delete-dialog.component';
import { IHistorial } from 'app/shared/model/historial.model';

@Injectable({ providedIn: 'root' })
export class HistorialResolve implements Resolve<IHistorial> {
  constructor(private service: HistorialService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IHistorial> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Historial>) => response.ok),
        map((historial: HttpResponse<Historial>) => historial.body)
      );
    }
    return of(new Historial());
  }
}

export const historialRoute: Routes = [
  {
    path: '',
    component: HistorialComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.historial.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: HistorialDetailComponent,
    resolve: {
      historial: HistorialResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.historial.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: HistorialUpdateComponent,
    resolve: {
      historial: HistorialResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.historial.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: HistorialUpdateComponent,
    resolve: {
      historial: HistorialResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.historial.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const historialPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: HistorialDeletePopupComponent,
    resolve: {
      historial: HistorialResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.historial.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
