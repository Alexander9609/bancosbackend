import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHistorial } from 'app/shared/model/historial.model';

@Component({
  selector: 'jhi-historial-detail',
  templateUrl: './historial-detail.component.html'
})
export class HistorialDetailComponent implements OnInit {
  historial: IHistorial;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ historial }) => {
      this.historial = historial;
    });
  }

  previousState() {
    window.history.back();
  }
}
