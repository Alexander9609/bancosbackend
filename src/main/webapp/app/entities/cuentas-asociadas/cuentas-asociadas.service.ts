import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';

type EntityResponseType = HttpResponse<ICuentasAsociadas>;
type EntityArrayResponseType = HttpResponse<ICuentasAsociadas[]>;

@Injectable({ providedIn: 'root' })
export class CuentasAsociadasService {
  public resourceUrl = SERVER_API_URL + 'api/cuentas-asociadas';

  constructor(protected http: HttpClient) {}

  create(cuentasAsociadas: ICuentasAsociadas): Observable<EntityResponseType> {
    return this.http.post<ICuentasAsociadas>(this.resourceUrl, cuentasAsociadas, { observe: 'response' });
  }

  update(cuentasAsociadas: ICuentasAsociadas): Observable<EntityResponseType> {
    return this.http.put<ICuentasAsociadas>(this.resourceUrl, cuentasAsociadas, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICuentasAsociadas>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICuentasAsociadas[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
