import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Nexos20SharedModule } from 'app/shared';
import {
  CuentasAsociadasComponent,
  CuentasAsociadasDetailComponent,
  CuentasAsociadasUpdateComponent,
  CuentasAsociadasDeletePopupComponent,
  CuentasAsociadasDeleteDialogComponent,
  cuentasAsociadasRoute,
  cuentasAsociadasPopupRoute
} from './';

const ENTITY_STATES = [...cuentasAsociadasRoute, ...cuentasAsociadasPopupRoute];

@NgModule({
  imports: [Nexos20SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    CuentasAsociadasComponent,
    CuentasAsociadasDetailComponent,
    CuentasAsociadasUpdateComponent,
    CuentasAsociadasDeleteDialogComponent,
    CuentasAsociadasDeletePopupComponent
  ],
  entryComponents: [
    CuentasAsociadasComponent,
    CuentasAsociadasUpdateComponent,
    CuentasAsociadasDeleteDialogComponent,
    CuentasAsociadasDeletePopupComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Nexos20CuentasAsociadasModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
