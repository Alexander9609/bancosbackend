import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';
import { CuentasAsociadasService } from './cuentas-asociadas.service';

@Component({
  selector: 'jhi-cuentas-asociadas-delete-dialog',
  templateUrl: './cuentas-asociadas-delete-dialog.component.html'
})
export class CuentasAsociadasDeleteDialogComponent {
  cuentasAsociadas: ICuentasAsociadas;

  constructor(
    protected cuentasAsociadasService: CuentasAsociadasService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.cuentasAsociadasService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'cuentasAsociadasListModification',
        content: 'Deleted an cuentasAsociadas'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-cuentas-asociadas-delete-popup',
  template: ''
})
export class CuentasAsociadasDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cuentasAsociadas }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CuentasAsociadasDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.cuentasAsociadas = cuentasAsociadas;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/cuentas-asociadas', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/cuentas-asociadas', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
