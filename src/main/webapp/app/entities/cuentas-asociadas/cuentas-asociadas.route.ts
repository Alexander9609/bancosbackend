import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { CuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';
import { CuentasAsociadasService } from './cuentas-asociadas.service';
import { CuentasAsociadasComponent } from './cuentas-asociadas.component';
import { CuentasAsociadasDetailComponent } from './cuentas-asociadas-detail.component';
import { CuentasAsociadasUpdateComponent } from './cuentas-asociadas-update.component';
import { CuentasAsociadasDeletePopupComponent } from './cuentas-asociadas-delete-dialog.component';
import { ICuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';

@Injectable({ providedIn: 'root' })
export class CuentasAsociadasResolve implements Resolve<ICuentasAsociadas> {
  constructor(private service: CuentasAsociadasService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICuentasAsociadas> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<CuentasAsociadas>) => response.ok),
        map((cuentasAsociadas: HttpResponse<CuentasAsociadas>) => cuentasAsociadas.body)
      );
    }
    return of(new CuentasAsociadas());
  }
}

export const cuentasAsociadasRoute: Routes = [
  {
    path: '',
    component: CuentasAsociadasComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.cuentasAsociadas.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CuentasAsociadasDetailComponent,
    resolve: {
      cuentasAsociadas: CuentasAsociadasResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.cuentasAsociadas.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CuentasAsociadasUpdateComponent,
    resolve: {
      cuentasAsociadas: CuentasAsociadasResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.cuentasAsociadas.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CuentasAsociadasUpdateComponent,
    resolve: {
      cuentasAsociadas: CuentasAsociadasResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.cuentasAsociadas.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const cuentasAsociadasPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CuentasAsociadasDeletePopupComponent,
    resolve: {
      cuentasAsociadas: CuentasAsociadasResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.cuentasAsociadas.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
