import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICuentasAsociadas, CuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';
import { CuentasAsociadasService } from './cuentas-asociadas.service';
import { IUser, UserService } from 'app/core';
import { ICuenta } from 'app/shared/model/cuenta.model';
import { CuentaService } from 'app/entities/cuenta';

@Component({
  selector: 'jhi-cuentas-asociadas-update',
  templateUrl: './cuentas-asociadas-update.component.html'
})
export class CuentasAsociadasUpdateComponent implements OnInit {
  cuentasAsociadas: ICuentasAsociadas;
  isSaving: boolean;

  users: IUser[];

  cuentas: ICuenta[];

  editForm = this.fb.group({
    id: [],
    aliasCuentaAsocia: [],
    user: [],
    cuenta: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected cuentasAsociadasService: CuentasAsociadasService,
    protected userService: UserService,
    protected cuentaService: CuentaService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ cuentasAsociadas }) => {
      this.updateForm(cuentasAsociadas);
      this.cuentasAsociadas = cuentasAsociadas;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.cuentaService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICuenta[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICuenta[]>) => response.body)
      )
      .subscribe((res: ICuenta[]) => (this.cuentas = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(cuentasAsociadas: ICuentasAsociadas) {
    this.editForm.patchValue({
      id: cuentasAsociadas.id,
      aliasCuentaAsocia: cuentasAsociadas.aliasCuentaAsocia,
      user: cuentasAsociadas.user,
      cuenta: cuentasAsociadas.cuenta
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const cuentasAsociadas = this.createFromForm();
    if (cuentasAsociadas.id !== undefined) {
      this.subscribeToSaveResponse(this.cuentasAsociadasService.update(cuentasAsociadas));
    } else {
      this.subscribeToSaveResponse(this.cuentasAsociadasService.create(cuentasAsociadas));
    }
  }

  private createFromForm(): ICuentasAsociadas {
    const entity = {
      ...new CuentasAsociadas(),
      id: this.editForm.get(['id']).value,
      aliasCuentaAsocia: this.editForm.get(['aliasCuentaAsocia']).value,
      user: this.editForm.get(['user']).value,
      cuenta: this.editForm.get(['cuenta']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICuentasAsociadas>>) {
    result.subscribe((res: HttpResponse<ICuentasAsociadas>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackCuentaById(index: number, item: ICuenta) {
    return item.id;
  }
}
