import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';

@Component({
  selector: 'jhi-cuentas-asociadas-detail',
  templateUrl: './cuentas-asociadas-detail.component.html'
})
export class CuentasAsociadasDetailComponent implements OnInit {
  cuentasAsociadas: ICuentasAsociadas;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ cuentasAsociadas }) => {
      this.cuentasAsociadas = cuentasAsociadas;
    });
  }

  previousState() {
    window.history.back();
  }
}
