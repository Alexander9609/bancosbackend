import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ITipo, Tipo } from 'app/shared/model/tipo.model';
import { TipoService } from './tipo.service';

@Component({
  selector: 'jhi-tipo-update',
  templateUrl: './tipo-update.component.html'
})
export class TipoUpdateComponent implements OnInit {
  tipo: ITipo;
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    tipoCuenta: []
  });

  constructor(protected tipoService: TipoService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ tipo }) => {
      this.updateForm(tipo);
      this.tipo = tipo;
    });
  }

  updateForm(tipo: ITipo) {
    this.editForm.patchValue({
      id: tipo.id,
      tipoCuenta: tipo.tipoCuenta
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const tipo = this.createFromForm();
    if (tipo.id !== undefined) {
      this.subscribeToSaveResponse(this.tipoService.update(tipo));
    } else {
      this.subscribeToSaveResponse(this.tipoService.create(tipo));
    }
  }

  private createFromForm(): ITipo {
    const entity = {
      ...new Tipo(),
      id: this.editForm.get(['id']).value,
      tipoCuenta: this.editForm.get(['tipoCuenta']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITipo>>) {
    result.subscribe((res: HttpResponse<ITipo>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
