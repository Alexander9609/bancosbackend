import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Nexos20SharedModule } from 'app/shared';
import {
  TipoComponent,
  TipoDetailComponent,
  TipoUpdateComponent,
  TipoDeletePopupComponent,
  TipoDeleteDialogComponent,
  tipoRoute,
  tipoPopupRoute
} from './';

const ENTITY_STATES = [...tipoRoute, ...tipoPopupRoute];

@NgModule({
  imports: [Nexos20SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [TipoComponent, TipoDetailComponent, TipoUpdateComponent, TipoDeleteDialogComponent, TipoDeletePopupComponent],
  entryComponents: [TipoComponent, TipoUpdateComponent, TipoDeleteDialogComponent, TipoDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Nexos20TipoModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
