import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Tipo } from 'app/shared/model/tipo.model';
import { TipoService } from './tipo.service';
import { TipoComponent } from './tipo.component';
import { TipoDetailComponent } from './tipo-detail.component';
import { TipoUpdateComponent } from './tipo-update.component';
import { TipoDeletePopupComponent } from './tipo-delete-dialog.component';
import { ITipo } from 'app/shared/model/tipo.model';

@Injectable({ providedIn: 'root' })
export class TipoResolve implements Resolve<ITipo> {
  constructor(private service: TipoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ITipo> {
    const id = route.params['id'] ? route.params['id'] : null;
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Tipo>) => response.ok),
        map((tipo: HttpResponse<Tipo>) => tipo.body)
      );
    }
    return of(new Tipo());
  }
}

export const tipoRoute: Routes = [
  {
    path: '',
    component: TipoComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.tipo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: TipoDetailComponent,
    resolve: {
      tipo: TipoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.tipo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: TipoUpdateComponent,
    resolve: {
      tipo: TipoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.tipo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: TipoUpdateComponent,
    resolve: {
      tipo: TipoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.tipo.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const tipoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: TipoDeletePopupComponent,
    resolve: {
      tipo: TipoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'nexos20App.tipo.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
