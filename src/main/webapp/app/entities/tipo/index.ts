export * from './tipo.service';
export * from './tipo-update.component';
export * from './tipo-delete-dialog.component';
export * from './tipo-detail.component';
export * from './tipo.component';
export * from './tipo.route';
