import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ITipo } from 'app/shared/model/tipo.model';
import { TipoService } from './tipo.service';

@Component({
  selector: 'jhi-tipo-delete-dialog',
  templateUrl: './tipo-delete-dialog.component.html'
})
export class TipoDeleteDialogComponent {
  tipo: ITipo;

  constructor(protected tipoService: TipoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.tipoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'tipoListModification',
        content: 'Deleted an tipo'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-tipo-delete-popup',
  template: ''
})
export class TipoDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ tipo }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(TipoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.tipo = tipo;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/tipo', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/tipo', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
