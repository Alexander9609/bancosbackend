import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { ICuenta, Cuenta } from 'app/shared/model/cuenta.model';
import { CuentaService } from './cuenta.service';
import { IUser, UserService } from 'app/core';
import { ITipo } from 'app/shared/model/tipo.model';
import { TipoService } from 'app/entities/tipo';
import { IBanco } from 'app/shared/model/banco.model';
import { BancoService } from 'app/entities/banco';

@Component({
  selector: 'jhi-cuenta-update',
  templateUrl: './cuenta-update.component.html'
})
export class CuentaUpdateComponent implements OnInit {
  cuenta: ICuenta;
  isSaving: boolean;

  users: IUser[];

  tipos: ITipo[];

  bancos: IBanco[];

  editForm = this.fb.group({
    id: [],
    numerCuenta: [],
    aliasCuenta: [],
    saldoCuenta: [],
    monedaCuenta: [],
    fechaCuenta: [],
    descripcion: [],
    user: [],
    tipocuenta: [],
    tipoNombreBanco: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected cuentaService: CuentaService,
    protected userService: UserService,
    protected tipoService: TipoService,
    protected bancoService: BancoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ cuenta }) => {
      this.updateForm(cuenta);
      this.cuenta = cuenta;
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.tipoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ITipo[]>) => mayBeOk.ok),
        map((response: HttpResponse<ITipo[]>) => response.body)
      )
      .subscribe((res: ITipo[]) => (this.tipos = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.bancoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IBanco[]>) => mayBeOk.ok),
        map((response: HttpResponse<IBanco[]>) => response.body)
      )
      .subscribe((res: IBanco[]) => (this.bancos = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(cuenta: ICuenta) {
    this.editForm.patchValue({
      id: cuenta.id,
      numerCuenta: cuenta.numerCuenta,
      aliasCuenta: cuenta.aliasCuenta,
      saldoCuenta: cuenta.saldoCuenta,
      monedaCuenta: cuenta.monedaCuenta,
      fechaCuenta: cuenta.fechaCuenta != null ? cuenta.fechaCuenta.format(DATE_TIME_FORMAT) : null,
      descripcion: cuenta.descripcion,
      user: cuenta.user,
      tipocuenta: cuenta.tipocuenta,
      tipoNombreBanco: cuenta.tipoNombreBanco
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const cuenta = this.createFromForm();
    if (cuenta.id !== undefined) {
      this.subscribeToSaveResponse(this.cuentaService.update(cuenta));
    } else {
      this.subscribeToSaveResponse(this.cuentaService.create(cuenta));
    }
  }

  private createFromForm(): ICuenta {
    const entity = {
      ...new Cuenta(),
      id: this.editForm.get(['id']).value,
      numerCuenta: this.editForm.get(['numerCuenta']).value,
      aliasCuenta: this.editForm.get(['aliasCuenta']).value,
      saldoCuenta: this.editForm.get(['saldoCuenta']).value,
      monedaCuenta: this.editForm.get(['monedaCuenta']).value,
      fechaCuenta:
        this.editForm.get(['fechaCuenta']).value != null ? moment(this.editForm.get(['fechaCuenta']).value, DATE_TIME_FORMAT) : undefined,
      descripcion: this.editForm.get(['descripcion']).value,
      user: this.editForm.get(['user']).value,
      tipocuenta: this.editForm.get(['tipocuenta']).value,
      tipoNombreBanco: this.editForm.get(['tipoNombreBanco']).value
    };
    return entity;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICuenta>>) {
    result.subscribe((res: HttpResponse<ICuenta>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackTipoById(index: number, item: ITipo) {
    return item.id;
  }

  trackBancoById(index: number, item: IBanco) {
    return item.id;
  }
}
