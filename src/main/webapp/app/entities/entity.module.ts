import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'banco',
        loadChildren: './banco/banco.module#Nexos20BancoModule'
      },
      {
        path: 'cuenta',
        loadChildren: './cuenta/cuenta.module#Nexos20CuentaModule'
      },
      {
        path: 'cuentas-asociadas',
        loadChildren: './cuentas-asociadas/cuentas-asociadas.module#Nexos20CuentasAsociadasModule'
      },
      {
        path: 'historial',
        loadChildren: './historial/historial.module#Nexos20HistorialModule'
      },
      {
        path: 'perfil',
        loadChildren: './perfil/perfil.module#Nexos20PerfilModule'
      },
      {
        path: 'tipo',
        loadChildren: './tipo/tipo.module#Nexos20TipoModule'
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Nexos20EntityModule {}
