import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { Nexos20SharedModule } from 'app/shared';
import {
  BancoComponent,
  BancoDetailComponent,
  BancoUpdateComponent,
  BancoDeletePopupComponent,
  BancoDeleteDialogComponent,
  bancoRoute,
  bancoPopupRoute
} from './';

const ENTITY_STATES = [...bancoRoute, ...bancoPopupRoute];

@NgModule({
  imports: [Nexos20SharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [BancoComponent, BancoDetailComponent, BancoUpdateComponent, BancoDeleteDialogComponent, BancoDeletePopupComponent],
  entryComponents: [BancoComponent, BancoUpdateComponent, BancoDeleteDialogComponent, BancoDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Nexos20BancoModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey !== undefined) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
