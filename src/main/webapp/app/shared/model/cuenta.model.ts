import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { ITipo } from 'app/shared/model/tipo.model';
import { IBanco } from 'app/shared/model/banco.model';

export interface ICuenta {
  id?: number;
  numerCuenta?: number;
  aliasCuenta?: string;
  saldoCuenta?: number;
  monedaCuenta?: string;
  fechaCuenta?: Moment;
  descripcion?: string;
  user?: IUser;
  tipocuenta?: ITipo;
  tipoNombreBanco?: IBanco;
}

export class Cuenta implements ICuenta {
  constructor(
    public id?: number,
    public numerCuenta?: number,
    public aliasCuenta?: string,
    public saldoCuenta?: number,
    public monedaCuenta?: string,
    public fechaCuenta?: Moment,
    public descripcion?: string,
    public user?: IUser,
    public tipocuenta?: ITipo,
    public tipoNombreBanco?: IBanco
  ) {}
}
