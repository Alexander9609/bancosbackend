import { IUser } from 'app/core/user/user.model';
import { ICuenta } from 'app/shared/model/cuenta.model';

export interface ICuentasAsociadas {
  id?: number;
  aliasCuentaAsocia?: string;
  user?: IUser;
  cuenta?: ICuenta;
}

export class CuentasAsociadas implements ICuentasAsociadas {
  constructor(public id?: number, public aliasCuentaAsocia?: string, public user?: IUser, public cuenta?: ICuenta) {}
}
