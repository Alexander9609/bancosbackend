import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Nexos20SharedLibsModule, Nexos20SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [Nexos20SharedLibsModule, Nexos20SharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [Nexos20SharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Nexos20SharedModule {
  static forRoot() {
    return {
      ngModule: Nexos20SharedModule
    };
  }
}
