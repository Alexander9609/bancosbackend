package com.nexos.bancos.web.rest;

import com.nexos.bancos.domain.Historial;
import com.nexos.bancos.service.HistorialService;
import com.nexos.bancos.web.rest.errors.BadRequestAlertException;
import com.nexos.bancos.service.dto.HistorialCriteria;
import com.nexos.bancos.service.HistorialQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nexos.bancos.domain.Historial}.
 */
@RestController
@RequestMapping("/api")
public class HistorialResource {

    private final Logger log = LoggerFactory.getLogger(HistorialResource.class);

    private static final String ENTITY_NAME = "historial";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HistorialService historialService;

    private final HistorialQueryService historialQueryService;

    public HistorialResource(HistorialService historialService, HistorialQueryService historialQueryService) {
        this.historialService = historialService;
        this.historialQueryService = historialQueryService;
    }

    /**
     * {@code POST  /historials} : Create a new historial.
     *
     * @param historial the historial to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new historial, or with status {@code 400 (Bad Request)} if the historial has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/historials")
    public ResponseEntity<Historial> createHistorial(@RequestBody Historial historial) throws URISyntaxException {
        log.debug("REST request to save Historial : {}", historial);
        if (historial.getId() != null) {
            throw new BadRequestAlertException("A new historial cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Historial result = historialService.save(historial);
        return ResponseEntity.created(new URI("/api/historials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /historials} : Updates an existing historial.
     *
     * @param historial the historial to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated historial,
     * or with status {@code 400 (Bad Request)} if the historial is not valid,
     * or with status {@code 500 (Internal Server Error)} if the historial couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/historials")
    public ResponseEntity<Historial> updateHistorial(@RequestBody Historial historial) throws URISyntaxException {
        log.debug("REST request to update Historial : {}", historial);
        if (historial.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Historial result = historialService.save(historial);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, historial.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /historials} : get all the historials.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of historials in body.
     */
    @GetMapping("/historials")
    public ResponseEntity<List<Historial>> getAllHistorials(HistorialCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Historials by criteria: {}", criteria);
        Page<Historial> page = historialQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /historials/count} : count all the historials.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/historials/count")
    public ResponseEntity<Long> countHistorials(HistorialCriteria criteria) {
        log.debug("REST request to count Historials by criteria: {}", criteria);
        return ResponseEntity.ok().body(historialQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /historials/:id} : get the "id" historial.
     *
     * @param id the id of the historial to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the historial, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/historials/{id}")
    public ResponseEntity<Historial> getHistorial(@PathVariable Long id) {
        log.debug("REST request to get Historial : {}", id);
        Optional<Historial> historial = historialService.findOne(id);
        return ResponseUtil.wrapOrNotFound(historial);
    }

    /**
     * {@code DELETE  /historials/:id} : delete the "id" historial.
     *
     * @param id the id of the historial to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/historials/{id}")
    public ResponseEntity<Void> deleteHistorial(@PathVariable Long id) {
        log.debug("REST request to delete Historial : {}", id);
        historialService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
