package com.nexos.bancos.web.rest;

import com.nexos.bancos.domain.CuentasAsociadas;
import com.nexos.bancos.service.CuentasAsociadasService;
import com.nexos.bancos.web.rest.errors.BadRequestAlertException;
import com.nexos.bancos.service.dto.CuentasAsociadasCriteria;
import com.nexos.bancos.service.CuentasAsociadasQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nexos.bancos.domain.CuentasAsociadas}.
 */
@RestController
@RequestMapping("/api")
public class CuentasAsociadasResource {

    private final Logger log = LoggerFactory.getLogger(CuentasAsociadasResource.class);

    private static final String ENTITY_NAME = "cuentasAsociadas";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CuentasAsociadasService cuentasAsociadasService;

    private final CuentasAsociadasQueryService cuentasAsociadasQueryService;

    public CuentasAsociadasResource(CuentasAsociadasService cuentasAsociadasService, CuentasAsociadasQueryService cuentasAsociadasQueryService) {
        this.cuentasAsociadasService = cuentasAsociadasService;
        this.cuentasAsociadasQueryService = cuentasAsociadasQueryService;
    }

    /**
     * {@code POST  /cuentas-asociadas} : Create a new cuentasAsociadas.
     *
     * @param cuentasAsociadas the cuentasAsociadas to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new cuentasAsociadas, or with status {@code 400 (Bad Request)} if the cuentasAsociadas has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/cuentas-asociadas")
    public ResponseEntity<CuentasAsociadas> createCuentasAsociadas(@RequestBody CuentasAsociadas cuentasAsociadas) throws URISyntaxException {
        log.debug("REST request to save CuentasAsociadas : {}", cuentasAsociadas);
        if (cuentasAsociadas.getId() != null) {
            throw new BadRequestAlertException("A new cuentasAsociadas cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CuentasAsociadas result = cuentasAsociadasService.save(cuentasAsociadas);
        return ResponseEntity.created(new URI("/api/cuentas-asociadas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /cuentas-asociadas} : Updates an existing cuentasAsociadas.
     *
     * @param cuentasAsociadas the cuentasAsociadas to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated cuentasAsociadas,
     * or with status {@code 400 (Bad Request)} if the cuentasAsociadas is not valid,
     * or with status {@code 500 (Internal Server Error)} if the cuentasAsociadas couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/cuentas-asociadas")
    public ResponseEntity<CuentasAsociadas> updateCuentasAsociadas(@RequestBody CuentasAsociadas cuentasAsociadas) throws URISyntaxException {
        log.debug("REST request to update CuentasAsociadas : {}", cuentasAsociadas);
        if (cuentasAsociadas.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        CuentasAsociadas result = cuentasAsociadasService.save(cuentasAsociadas);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, cuentasAsociadas.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /cuentas-asociadas} : get all the cuentasAsociadas.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of cuentasAsociadas in body.
     */
    @GetMapping("/cuentas-asociadas")
    public ResponseEntity<List<CuentasAsociadas>> getAllCuentasAsociadas(CuentasAsociadasCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get CuentasAsociadas by criteria: {}", criteria);
        Page<CuentasAsociadas> page = cuentasAsociadasQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /cuentas-asociadas/count} : count all the cuentasAsociadas.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/cuentas-asociadas/count")
    public ResponseEntity<Long> countCuentasAsociadas(CuentasAsociadasCriteria criteria) {
        log.debug("REST request to count CuentasAsociadas by criteria: {}", criteria);
        return ResponseEntity.ok().body(cuentasAsociadasQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /cuentas-asociadas/:id} : get the "id" cuentasAsociadas.
     *
     * @param id the id of the cuentasAsociadas to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the cuentasAsociadas, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/cuentas-asociadas/{id}")
    public ResponseEntity<CuentasAsociadas> getCuentasAsociadas(@PathVariable Long id) {
        log.debug("REST request to get CuentasAsociadas : {}", id);
        Optional<CuentasAsociadas> cuentasAsociadas = cuentasAsociadasService.findOne(id);
        return ResponseUtil.wrapOrNotFound(cuentasAsociadas);
    }

    /**
     * {@code DELETE  /cuentas-asociadas/:id} : delete the "id" cuentasAsociadas.
     *
     * @param id the id of the cuentasAsociadas to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/cuentas-asociadas/{id}")
    public ResponseEntity<Void> deleteCuentasAsociadas(@PathVariable Long id) {
        log.debug("REST request to delete CuentasAsociadas : {}", id);
        cuentasAsociadasService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
