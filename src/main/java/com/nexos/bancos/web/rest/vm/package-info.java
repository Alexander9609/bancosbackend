/**
 * View Models used by Spring MVC REST controllers.
 */
package com.nexos.bancos.web.rest.vm;
