package com.nexos.bancos.web.rest;

import com.nexos.bancos.domain.Tipo;
import com.nexos.bancos.service.TipoService;
import com.nexos.bancos.web.rest.errors.BadRequestAlertException;
import com.nexos.bancos.service.dto.TipoCriteria;
import com.nexos.bancos.service.TipoQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.nexos.bancos.domain.Tipo}.
 */
@RestController
@RequestMapping("/api")
public class TipoResource {

    private final Logger log = LoggerFactory.getLogger(TipoResource.class);

    private static final String ENTITY_NAME = "tipo";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final TipoService tipoService;

    private final TipoQueryService tipoQueryService;

    public TipoResource(TipoService tipoService, TipoQueryService tipoQueryService) {
        this.tipoService = tipoService;
        this.tipoQueryService = tipoQueryService;
    }

    /**
     * {@code POST  /tipos} : Create a new tipo.
     *
     * @param tipo the tipo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new tipo, or with status {@code 400 (Bad Request)} if the tipo has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/tipos")
    public ResponseEntity<Tipo> createTipo(@RequestBody Tipo tipo) throws URISyntaxException {
        log.debug("REST request to save Tipo : {}", tipo);
        if (tipo.getId() != null) {
            throw new BadRequestAlertException("A new tipo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tipo result = tipoService.save(tipo);
        return ResponseEntity.created(new URI("/api/tipos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /tipos} : Updates an existing tipo.
     *
     * @param tipo the tipo to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated tipo,
     * or with status {@code 400 (Bad Request)} if the tipo is not valid,
     * or with status {@code 500 (Internal Server Error)} if the tipo couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/tipos")
    public ResponseEntity<Tipo> updateTipo(@RequestBody Tipo tipo) throws URISyntaxException {
        log.debug("REST request to update Tipo : {}", tipo);
        if (tipo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Tipo result = tipoService.save(tipo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, tipo.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /tipos} : get all the tipos.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of tipos in body.
     */
    @GetMapping("/tipos")
    public ResponseEntity<List<Tipo>> getAllTipos(TipoCriteria criteria, Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get Tipos by criteria: {}", criteria);
        Page<Tipo> page = tipoQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
    * {@code GET  /tipos/count} : count all the tipos.
    *
    * @param criteria the criteria which the requested entities should match.
    * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
    */
    @GetMapping("/tipos/count")
    public ResponseEntity<Long> countTipos(TipoCriteria criteria) {
        log.debug("REST request to count Tipos by criteria: {}", criteria);
        return ResponseEntity.ok().body(tipoQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /tipos/:id} : get the "id" tipo.
     *
     * @param id the id of the tipo to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the tipo, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/tipos/{id}")
    public ResponseEntity<Tipo> getTipo(@PathVariable Long id) {
        log.debug("REST request to get Tipo : {}", id);
        Optional<Tipo> tipo = tipoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(tipo);
    }

    /**
     * {@code DELETE  /tipos/:id} : delete the "id" tipo.
     *
     * @param id the id of the tipo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/tipos/{id}")
    public ResponseEntity<Void> deleteTipo(@PathVariable Long id) {
        log.debug("REST request to delete Tipo : {}", id);
        tipoService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
