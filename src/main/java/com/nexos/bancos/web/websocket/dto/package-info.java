/**
 * Data Access Objects used by WebSocket services.
 */
package com.nexos.bancos.web.websocket.dto;
