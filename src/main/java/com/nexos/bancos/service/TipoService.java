package com.nexos.bancos.service;

import com.nexos.bancos.domain.Tipo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Tipo}.
 */
public interface TipoService {

    /**
     * Save a tipo.
     *
     * @param tipo the entity to save.
     * @return the persisted entity.
     */
    Tipo save(Tipo tipo);

    /**
     * Get all the tipos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Tipo> findAll(Pageable pageable);


    /**
     * Get the "id" tipo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Tipo> findOne(Long id);

    /**
     * Delete the "id" tipo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
