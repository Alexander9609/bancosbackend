package com.nexos.bancos.service;

import com.nexos.bancos.domain.Historial;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Historial}.
 */
public interface HistorialService {

    /**
     * Save a historial.
     *
     * @param historial the entity to save.
     * @return the persisted entity.
     */
    Historial save(Historial historial);

    /**
     * Get all the historials.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Historial> findAll(Pageable pageable);


    /**
     * Get the "id" historial.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Historial> findOne(Long id);

    /**
     * Delete the "id" historial.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
