package com.nexos.bancos.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.nexos.bancos.domain.Historial;
import com.nexos.bancos.domain.*; // for static metamodels
import com.nexos.bancos.repository.HistorialRepository;
import com.nexos.bancos.service.dto.HistorialCriteria;

/**
 * Service for executing complex queries for {@link Historial} entities in the database.
 * The main input is a {@link HistorialCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Historial} or a {@link Page} of {@link Historial} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class HistorialQueryService extends QueryService<Historial> {

    private final Logger log = LoggerFactory.getLogger(HistorialQueryService.class);

    private final HistorialRepository historialRepository;

    public HistorialQueryService(HistorialRepository historialRepository) {
        this.historialRepository = historialRepository;
    }

    /**
     * Return a {@link List} of {@link Historial} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Historial> findByCriteria(HistorialCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Historial> specification = createSpecification(criteria);
        return historialRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Historial} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Historial> findByCriteria(HistorialCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Historial> specification = createSpecification(criteria);
        return historialRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(HistorialCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Historial> specification = createSpecification(criteria);
        return historialRepository.count(specification);
    }

    /**
     * Function to convert HistorialCriteria to a {@link Specification}.
     */
    private Specification<Historial> createSpecification(HistorialCriteria criteria) {
        Specification<Historial> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Historial_.id));
            }
            if (criteria.getMontoHistorial() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getMontoHistorial(), Historial_.montoHistorial));
            }
            if (criteria.getDescripcionHistorial() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescripcionHistorial(), Historial_.descripcionHistorial));
            }
            if (criteria.getIdTransaccion() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdTransaccion(), Historial_.idTransaccion));
            }
            if (criteria.getFechaHistorial() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFechaHistorial(), Historial_.fechaHistorial));
            }
            if (criteria.getCuentaId() != null) {
                specification = specification.and(buildSpecification(criteria.getCuentaId(),
                    root -> root.join(Historial_.cuenta, JoinType.LEFT).get(Cuenta_.id)));
            }
            if (criteria.getCuentasAsociadasId() != null) {
                specification = specification.and(buildSpecification(criteria.getCuentasAsociadasId(),
                    root -> root.join(Historial_.cuentasAsociadas, JoinType.LEFT).get(CuentasAsociadas_.id)));
            }
        }
        return specification;
    }
}
