package com.nexos.bancos.service;

import com.nexos.bancos.domain.Banco;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link Banco}.
 */
public interface BancoService {

    /**
     * Save a banco.
     *
     * @param banco the entity to save.
     * @return the persisted entity.
     */
    Banco save(Banco banco);

    /**
     * Get all the bancos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Banco> findAll(Pageable pageable);


    /**
     * Get the "id" banco.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Banco> findOne(Long id);

    /**
     * Delete the "id" banco.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
