package com.nexos.bancos.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.nexos.bancos.domain.Cuenta;
import com.nexos.bancos.domain.*; // for static metamodels
import com.nexos.bancos.repository.CuentaRepository;
import com.nexos.bancos.service.dto.CuentaCriteria;

/**
 * Service for executing complex queries for {@link Cuenta} entities in the database.
 * The main input is a {@link CuentaCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Cuenta} or a {@link Page} of {@link Cuenta} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CuentaQueryService extends QueryService<Cuenta> {

    private final Logger log = LoggerFactory.getLogger(CuentaQueryService.class);

    private final CuentaRepository cuentaRepository;

    public CuentaQueryService(CuentaRepository cuentaRepository) {
        this.cuentaRepository = cuentaRepository;
    }

    /**
     * Return a {@link List} of {@link Cuenta} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Cuenta> findByCriteria(CuentaCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Cuenta> specification = createSpecification(criteria);
        return cuentaRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Cuenta} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Cuenta> findByCriteria(CuentaCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Cuenta> specification = createSpecification(criteria);
        return cuentaRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CuentaCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Cuenta> specification = createSpecification(criteria);
        return cuentaRepository.count(specification);
    }

    /**
     * Function to convert CuentaCriteria to a {@link Specification}.
     */
    private Specification<Cuenta> createSpecification(CuentaCriteria criteria) {
        Specification<Cuenta> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Cuenta_.id));
            }
            if (criteria.getNumerCuenta() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getNumerCuenta(), Cuenta_.numerCuenta));
            }
            if (criteria.getAliasCuenta() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAliasCuenta(), Cuenta_.aliasCuenta));
            }
            if (criteria.getSaldoCuenta() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSaldoCuenta(), Cuenta_.saldoCuenta));
            }
            if (criteria.getMonedaCuenta() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMonedaCuenta(), Cuenta_.monedaCuenta));
            }
            if (criteria.getFechaCuenta() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFechaCuenta(), Cuenta_.fechaCuenta));
            }
            if (criteria.getDescripcion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescripcion(), Cuenta_.descripcion));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Cuenta_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getTipocuentaId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipocuentaId(),
                    root -> root.join(Cuenta_.tipocuenta, JoinType.LEFT).get(Tipo_.id)));
            }
            if (criteria.getTipoNombreBancoId() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoNombreBancoId(),
                    root -> root.join(Cuenta_.tipoNombreBanco, JoinType.LEFT).get(Banco_.id)));
            }
        }
        return specification;
    }
}
