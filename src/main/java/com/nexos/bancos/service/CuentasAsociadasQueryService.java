package com.nexos.bancos.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.nexos.bancos.domain.CuentasAsociadas;
import com.nexos.bancos.domain.*; // for static metamodels
import com.nexos.bancos.repository.CuentasAsociadasRepository;
import com.nexos.bancos.service.dto.CuentasAsociadasCriteria;

/**
 * Service for executing complex queries for {@link CuentasAsociadas} entities in the database.
 * The main input is a {@link CuentasAsociadasCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link CuentasAsociadas} or a {@link Page} of {@link CuentasAsociadas} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class CuentasAsociadasQueryService extends QueryService<CuentasAsociadas> {

    private final Logger log = LoggerFactory.getLogger(CuentasAsociadasQueryService.class);

    private final CuentasAsociadasRepository cuentasAsociadasRepository;

    public CuentasAsociadasQueryService(CuentasAsociadasRepository cuentasAsociadasRepository) {
        this.cuentasAsociadasRepository = cuentasAsociadasRepository;
    }

    /**
     * Return a {@link List} of {@link CuentasAsociadas} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<CuentasAsociadas> findByCriteria(CuentasAsociadasCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<CuentasAsociadas> specification = createSpecification(criteria);
        return cuentasAsociadasRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link CuentasAsociadas} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<CuentasAsociadas> findByCriteria(CuentasAsociadasCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<CuentasAsociadas> specification = createSpecification(criteria);
        return cuentasAsociadasRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(CuentasAsociadasCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<CuentasAsociadas> specification = createSpecification(criteria);
        return cuentasAsociadasRepository.count(specification);
    }

    /**
     * Function to convert CuentasAsociadasCriteria to a {@link Specification}.
     */
    private Specification<CuentasAsociadas> createSpecification(CuentasAsociadasCriteria criteria) {
        Specification<CuentasAsociadas> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), CuentasAsociadas_.id));
            }
            if (criteria.getAliasCuentaAsocia() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAliasCuentaAsocia(), CuentasAsociadas_.aliasCuentaAsocia));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(CuentasAsociadas_.user, JoinType.LEFT).get(User_.id)));
            }
            if (criteria.getCuentaId() != null) {
                specification = specification.and(buildSpecification(criteria.getCuentaId(),
                    root -> root.join(CuentasAsociadas_.cuenta, JoinType.LEFT).get(Cuenta_.id)));
            }
        }
        return specification;
    }
}
