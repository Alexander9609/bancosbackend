package com.nexos.bancos.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.nexos.bancos.domain.Tipo;
import com.nexos.bancos.domain.*; // for static metamodels
import com.nexos.bancos.repository.TipoRepository;
import com.nexos.bancos.service.dto.TipoCriteria;

/**
 * Service for executing complex queries for {@link Tipo} entities in the database.
 * The main input is a {@link TipoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Tipo} or a {@link Page} of {@link Tipo} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class TipoQueryService extends QueryService<Tipo> {

    private final Logger log = LoggerFactory.getLogger(TipoQueryService.class);

    private final TipoRepository tipoRepository;

    public TipoQueryService(TipoRepository tipoRepository) {
        this.tipoRepository = tipoRepository;
    }

    /**
     * Return a {@link List} of {@link Tipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Tipo> findByCriteria(TipoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Tipo> specification = createSpecification(criteria);
        return tipoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Tipo} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Tipo> findByCriteria(TipoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Tipo> specification = createSpecification(criteria);
        return tipoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(TipoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Tipo> specification = createSpecification(criteria);
        return tipoRepository.count(specification);
    }

    /**
     * Function to convert TipoCriteria to a {@link Specification}.
     */
    private Specification<Tipo> createSpecification(TipoCriteria criteria) {
        Specification<Tipo> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Tipo_.id));
            }
            if (criteria.getTipoCuenta() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoCuenta(), Tipo_.tipoCuenta));
            }
        }
        return specification;
    }
}
