package com.nexos.bancos.service;

import com.nexos.bancos.domain.CuentasAsociadas;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link CuentasAsociadas}.
 */
public interface CuentasAsociadasService {

    /**
     * Save a cuentasAsociadas.
     *
     * @param cuentasAsociadas the entity to save.
     * @return the persisted entity.
     */
    CuentasAsociadas save(CuentasAsociadas cuentasAsociadas);

    /**
     * Get all the cuentasAsociadas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<CuentasAsociadas> findAll(Pageable pageable);


    /**
     * Get the "id" cuentasAsociadas.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<CuentasAsociadas> findOne(Long id);

    /**
     * Delete the "id" cuentasAsociadas.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
