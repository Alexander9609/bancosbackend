package com.nexos.bancos.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.nexos.bancos.domain.Perfil;
import com.nexos.bancos.domain.*; // for static metamodels
import com.nexos.bancos.repository.PerfilRepository;
import com.nexos.bancos.service.dto.PerfilCriteria;

/**
 * Service for executing complex queries for {@link Perfil} entities in the database.
 * The main input is a {@link PerfilCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Perfil} or a {@link Page} of {@link Perfil} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PerfilQueryService extends QueryService<Perfil> {

    private final Logger log = LoggerFactory.getLogger(PerfilQueryService.class);

    private final PerfilRepository perfilRepository;

    public PerfilQueryService(PerfilRepository perfilRepository) {
        this.perfilRepository = perfilRepository;
    }

    /**
     * Return a {@link List} of {@link Perfil} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Perfil> findByCriteria(PerfilCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Perfil> specification = createSpecification(criteria);
        return perfilRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Perfil} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Perfil> findByCriteria(PerfilCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Perfil> specification = createSpecification(criteria);
        return perfilRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(PerfilCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Perfil> specification = createSpecification(criteria);
        return perfilRepository.count(specification);
    }

    /**
     * Function to convert PerfilCriteria to a {@link Specification}.
     */
    private Specification<Perfil> createSpecification(PerfilCriteria criteria) {
        Specification<Perfil> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Perfil_.id));
            }
            if (criteria.getTipoIdentificacion() != null) {
                specification = specification.and(buildSpecification(criteria.getTipoIdentificacion(), Perfil_.tipoIdentificacion));
            }
            if (criteria.getIdentificacion() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getIdentificacion(), Perfil_.identificacion));
            }
            if (criteria.getEdad() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEdad(), Perfil_.edad));
            }
            if (criteria.getSexo() != null) {
                specification = specification.and(buildSpecification(criteria.getSexo(), Perfil_.sexo));
            }
            if (criteria.getDireccion() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDireccion(), Perfil_.direccion));
            }
            if (criteria.getFechaNacimiento() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFechaNacimiento(), Perfil_.fechaNacimiento));
            }
            if (criteria.getNacionalidad() != null) {
                specification = specification.and(buildSpecification(criteria.getNacionalidad(), Perfil_.nacionalidad));
            }
            if (criteria.getTelefono() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getTelefono(), Perfil_.telefono));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildSpecification(criteria.getUserId(),
                    root -> root.join(Perfil_.user, JoinType.LEFT).get(User_.id)));
            }
        }
        return specification;
    }
}
