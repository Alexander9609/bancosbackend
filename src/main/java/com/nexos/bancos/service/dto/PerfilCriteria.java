package com.nexos.bancos.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import com.nexos.bancos.domain.enumeration.Identificacion;
import com.nexos.bancos.domain.enumeration.Sexo;
import com.nexos.bancos.domain.enumeration.Nacionalidad;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.nexos.bancos.domain.Perfil} entity. This class is used
 * in {@link com.nexos.bancos.web.rest.PerfilResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /perfils?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PerfilCriteria implements Serializable, Criteria {
    /**
     * Class for filtering Identificacion
     */
    public static class IdentificacionFilter extends Filter<Identificacion> {

        public IdentificacionFilter() {
        }

        public IdentificacionFilter(IdentificacionFilter filter) {
            super(filter);
        }

        @Override
        public IdentificacionFilter copy() {
            return new IdentificacionFilter(this);
        }

    }
    /**
     * Class for filtering Sexo
     */
    public static class SexoFilter extends Filter<Sexo> {

        public SexoFilter() {
        }

        public SexoFilter(SexoFilter filter) {
            super(filter);
        }

        @Override
        public SexoFilter copy() {
            return new SexoFilter(this);
        }

    }
    /**
     * Class for filtering Nacionalidad
     */
    public static class NacionalidadFilter extends Filter<Nacionalidad> {

        public NacionalidadFilter() {
        }

        public NacionalidadFilter(NacionalidadFilter filter) {
            super(filter);
        }

        @Override
        public NacionalidadFilter copy() {
            return new NacionalidadFilter(this);
        }

    }

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IdentificacionFilter tipoIdentificacion;

    private IntegerFilter identificacion;

    private IntegerFilter edad;

    private SexoFilter sexo;

    private StringFilter direccion;

    private InstantFilter fechaNacimiento;

    private NacionalidadFilter nacionalidad;

    private IntegerFilter telefono;

    private LongFilter userId;

    public PerfilCriteria(){
    }

    public PerfilCriteria(PerfilCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.tipoIdentificacion = other.tipoIdentificacion == null ? null : other.tipoIdentificacion.copy();
        this.identificacion = other.identificacion == null ? null : other.identificacion.copy();
        this.edad = other.edad == null ? null : other.edad.copy();
        this.sexo = other.sexo == null ? null : other.sexo.copy();
        this.direccion = other.direccion == null ? null : other.direccion.copy();
        this.fechaNacimiento = other.fechaNacimiento == null ? null : other.fechaNacimiento.copy();
        this.nacionalidad = other.nacionalidad == null ? null : other.nacionalidad.copy();
        this.telefono = other.telefono == null ? null : other.telefono.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
    }

    @Override
    public PerfilCriteria copy() {
        return new PerfilCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IdentificacionFilter getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(IdentificacionFilter tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public IntegerFilter getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(IntegerFilter identificacion) {
        this.identificacion = identificacion;
    }

    public IntegerFilter getEdad() {
        return edad;
    }

    public void setEdad(IntegerFilter edad) {
        this.edad = edad;
    }

    public SexoFilter getSexo() {
        return sexo;
    }

    public void setSexo(SexoFilter sexo) {
        this.sexo = sexo;
    }

    public StringFilter getDireccion() {
        return direccion;
    }

    public void setDireccion(StringFilter direccion) {
        this.direccion = direccion;
    }

    public InstantFilter getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(InstantFilter fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public NacionalidadFilter getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(NacionalidadFilter nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public IntegerFilter getTelefono() {
        return telefono;
    }

    public void setTelefono(IntegerFilter telefono) {
        this.telefono = telefono;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final PerfilCriteria that = (PerfilCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tipoIdentificacion, that.tipoIdentificacion) &&
            Objects.equals(identificacion, that.identificacion) &&
            Objects.equals(edad, that.edad) &&
            Objects.equals(sexo, that.sexo) &&
            Objects.equals(direccion, that.direccion) &&
            Objects.equals(fechaNacimiento, that.fechaNacimiento) &&
            Objects.equals(nacionalidad, that.nacionalidad) &&
            Objects.equals(telefono, that.telefono) &&
            Objects.equals(userId, that.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tipoIdentificacion,
        identificacion,
        edad,
        sexo,
        direccion,
        fechaNacimiento,
        nacionalidad,
        telefono,
        userId
        );
    }

    @Override
    public String toString() {
        return "PerfilCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tipoIdentificacion != null ? "tipoIdentificacion=" + tipoIdentificacion + ", " : "") +
                (identificacion != null ? "identificacion=" + identificacion + ", " : "") +
                (edad != null ? "edad=" + edad + ", " : "") +
                (sexo != null ? "sexo=" + sexo + ", " : "") +
                (direccion != null ? "direccion=" + direccion + ", " : "") +
                (fechaNacimiento != null ? "fechaNacimiento=" + fechaNacimiento + ", " : "") +
                (nacionalidad != null ? "nacionalidad=" + nacionalidad + ", " : "") +
                (telefono != null ? "telefono=" + telefono + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
