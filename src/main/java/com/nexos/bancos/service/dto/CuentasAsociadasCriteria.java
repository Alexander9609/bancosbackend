package com.nexos.bancos.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.nexos.bancos.domain.CuentasAsociadas} entity. This class is used
 * in {@link com.nexos.bancos.web.rest.CuentasAsociadasResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cuentas-asociadas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CuentasAsociadasCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter aliasCuentaAsocia;

    private LongFilter userId;

    private LongFilter cuentaId;

    public CuentasAsociadasCriteria(){
    }

    public CuentasAsociadasCriteria(CuentasAsociadasCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.aliasCuentaAsocia = other.aliasCuentaAsocia == null ? null : other.aliasCuentaAsocia.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.cuentaId = other.cuentaId == null ? null : other.cuentaId.copy();
    }

    @Override
    public CuentasAsociadasCriteria copy() {
        return new CuentasAsociadasCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getAliasCuentaAsocia() {
        return aliasCuentaAsocia;
    }

    public void setAliasCuentaAsocia(StringFilter aliasCuentaAsocia) {
        this.aliasCuentaAsocia = aliasCuentaAsocia;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getCuentaId() {
        return cuentaId;
    }

    public void setCuentaId(LongFilter cuentaId) {
        this.cuentaId = cuentaId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CuentasAsociadasCriteria that = (CuentasAsociadasCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(aliasCuentaAsocia, that.aliasCuentaAsocia) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(cuentaId, that.cuentaId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        aliasCuentaAsocia,
        userId,
        cuentaId
        );
    }

    @Override
    public String toString() {
        return "CuentasAsociadasCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (aliasCuentaAsocia != null ? "aliasCuentaAsocia=" + aliasCuentaAsocia + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (cuentaId != null ? "cuentaId=" + cuentaId + ", " : "") +
            "}";
    }

}
