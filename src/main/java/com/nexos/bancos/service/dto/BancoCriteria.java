package com.nexos.bancos.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.nexos.bancos.domain.Banco} entity. This class is used
 * in {@link com.nexos.bancos.web.rest.BancoResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /bancos?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BancoCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private StringFilter tipoNombreBanco;

    public BancoCriteria(){
    }

    public BancoCriteria(BancoCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.tipoNombreBanco = other.tipoNombreBanco == null ? null : other.tipoNombreBanco.copy();
    }

    @Override
    public BancoCriteria copy() {
        return new BancoCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getTipoNombreBanco() {
        return tipoNombreBanco;
    }

    public void setTipoNombreBanco(StringFilter tipoNombreBanco) {
        this.tipoNombreBanco = tipoNombreBanco;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BancoCriteria that = (BancoCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(tipoNombreBanco, that.tipoNombreBanco);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        tipoNombreBanco
        );
    }

    @Override
    public String toString() {
        return "BancoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (tipoNombreBanco != null ? "tipoNombreBanco=" + tipoNombreBanco + ", " : "") +
            "}";
    }

}
