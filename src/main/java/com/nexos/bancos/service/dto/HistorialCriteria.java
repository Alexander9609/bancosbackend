package com.nexos.bancos.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.nexos.bancos.domain.Historial} entity. This class is used
 * in {@link com.nexos.bancos.web.rest.HistorialResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /historials?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class HistorialCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter montoHistorial;

    private StringFilter descripcionHistorial;

    private IntegerFilter idTransaccion;

    private InstantFilter fechaHistorial;

    private LongFilter cuentaId;

    private LongFilter cuentasAsociadasId;

    public HistorialCriteria(){
    }

    public HistorialCriteria(HistorialCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.montoHistorial = other.montoHistorial == null ? null : other.montoHistorial.copy();
        this.descripcionHistorial = other.descripcionHistorial == null ? null : other.descripcionHistorial.copy();
        this.idTransaccion = other.idTransaccion == null ? null : other.idTransaccion.copy();
        this.fechaHistorial = other.fechaHistorial == null ? null : other.fechaHistorial.copy();
        this.cuentaId = other.cuentaId == null ? null : other.cuentaId.copy();
        this.cuentasAsociadasId = other.cuentasAsociadasId == null ? null : other.cuentasAsociadasId.copy();
    }

    @Override
    public HistorialCriteria copy() {
        return new HistorialCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getMontoHistorial() {
        return montoHistorial;
    }

    public void setMontoHistorial(IntegerFilter montoHistorial) {
        this.montoHistorial = montoHistorial;
    }

    public StringFilter getDescripcionHistorial() {
        return descripcionHistorial;
    }

    public void setDescripcionHistorial(StringFilter descripcionHistorial) {
        this.descripcionHistorial = descripcionHistorial;
    }

    public IntegerFilter getIdTransaccion() {
        return idTransaccion;
    }

    public void setIdTransaccion(IntegerFilter idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public InstantFilter getFechaHistorial() {
        return fechaHistorial;
    }

    public void setFechaHistorial(InstantFilter fechaHistorial) {
        this.fechaHistorial = fechaHistorial;
    }

    public LongFilter getCuentaId() {
        return cuentaId;
    }

    public void setCuentaId(LongFilter cuentaId) {
        this.cuentaId = cuentaId;
    }

    public LongFilter getCuentasAsociadasId() {
        return cuentasAsociadasId;
    }

    public void setCuentasAsociadasId(LongFilter cuentasAsociadasId) {
        this.cuentasAsociadasId = cuentasAsociadasId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final HistorialCriteria that = (HistorialCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(montoHistorial, that.montoHistorial) &&
            Objects.equals(descripcionHistorial, that.descripcionHistorial) &&
            Objects.equals(idTransaccion, that.idTransaccion) &&
            Objects.equals(fechaHistorial, that.fechaHistorial) &&
            Objects.equals(cuentaId, that.cuentaId) &&
            Objects.equals(cuentasAsociadasId, that.cuentasAsociadasId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        montoHistorial,
        descripcionHistorial,
        idTransaccion,
        fechaHistorial,
        cuentaId,
        cuentasAsociadasId
        );
    }

    @Override
    public String toString() {
        return "HistorialCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (montoHistorial != null ? "montoHistorial=" + montoHistorial + ", " : "") +
                (descripcionHistorial != null ? "descripcionHistorial=" + descripcionHistorial + ", " : "") +
                (idTransaccion != null ? "idTransaccion=" + idTransaccion + ", " : "") +
                (fechaHistorial != null ? "fechaHistorial=" + fechaHistorial + ", " : "") +
                (cuentaId != null ? "cuentaId=" + cuentaId + ", " : "") +
                (cuentasAsociadasId != null ? "cuentasAsociadasId=" + cuentasAsociadasId + ", " : "") +
            "}";
    }

}
