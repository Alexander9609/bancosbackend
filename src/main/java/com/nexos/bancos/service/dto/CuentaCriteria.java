package com.nexos.bancos.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.nexos.bancos.domain.Cuenta} entity. This class is used
 * in {@link com.nexos.bancos.web.rest.CuentaResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /cuentas?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class CuentaCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private IntegerFilter numerCuenta;

    private StringFilter aliasCuenta;

    private IntegerFilter saldoCuenta;

    private StringFilter monedaCuenta;

    private InstantFilter fechaCuenta;

    private StringFilter descripcion;

    private LongFilter userId;

    private LongFilter tipocuentaId;

    private LongFilter tipoNombreBancoId;

    public CuentaCriteria(){
    }

    public CuentaCriteria(CuentaCriteria other){
        this.id = other.id == null ? null : other.id.copy();
        this.numerCuenta = other.numerCuenta == null ? null : other.numerCuenta.copy();
        this.aliasCuenta = other.aliasCuenta == null ? null : other.aliasCuenta.copy();
        this.saldoCuenta = other.saldoCuenta == null ? null : other.saldoCuenta.copy();
        this.monedaCuenta = other.monedaCuenta == null ? null : other.monedaCuenta.copy();
        this.fechaCuenta = other.fechaCuenta == null ? null : other.fechaCuenta.copy();
        this.descripcion = other.descripcion == null ? null : other.descripcion.copy();
        this.userId = other.userId == null ? null : other.userId.copy();
        this.tipocuentaId = other.tipocuentaId == null ? null : other.tipocuentaId.copy();
        this.tipoNombreBancoId = other.tipoNombreBancoId == null ? null : other.tipoNombreBancoId.copy();
    }

    @Override
    public CuentaCriteria copy() {
        return new CuentaCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public IntegerFilter getNumerCuenta() {
        return numerCuenta;
    }

    public void setNumerCuenta(IntegerFilter numerCuenta) {
        this.numerCuenta = numerCuenta;
    }

    public StringFilter getAliasCuenta() {
        return aliasCuenta;
    }

    public void setAliasCuenta(StringFilter aliasCuenta) {
        this.aliasCuenta = aliasCuenta;
    }

    public IntegerFilter getSaldoCuenta() {
        return saldoCuenta;
    }

    public void setSaldoCuenta(IntegerFilter saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
    }

    public StringFilter getMonedaCuenta() {
        return monedaCuenta;
    }

    public void setMonedaCuenta(StringFilter monedaCuenta) {
        this.monedaCuenta = monedaCuenta;
    }

    public InstantFilter getFechaCuenta() {
        return fechaCuenta;
    }

    public void setFechaCuenta(InstantFilter fechaCuenta) {
        this.fechaCuenta = fechaCuenta;
    }

    public StringFilter getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(StringFilter descripcion) {
        this.descripcion = descripcion;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    public LongFilter getTipocuentaId() {
        return tipocuentaId;
    }

    public void setTipocuentaId(LongFilter tipocuentaId) {
        this.tipocuentaId = tipocuentaId;
    }

    public LongFilter getTipoNombreBancoId() {
        return tipoNombreBancoId;
    }

    public void setTipoNombreBancoId(LongFilter tipoNombreBancoId) {
        this.tipoNombreBancoId = tipoNombreBancoId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final CuentaCriteria that = (CuentaCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(numerCuenta, that.numerCuenta) &&
            Objects.equals(aliasCuenta, that.aliasCuenta) &&
            Objects.equals(saldoCuenta, that.saldoCuenta) &&
            Objects.equals(monedaCuenta, that.monedaCuenta) &&
            Objects.equals(fechaCuenta, that.fechaCuenta) &&
            Objects.equals(descripcion, that.descripcion) &&
            Objects.equals(userId, that.userId) &&
            Objects.equals(tipocuentaId, that.tipocuentaId) &&
            Objects.equals(tipoNombreBancoId, that.tipoNombreBancoId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        numerCuenta,
        aliasCuenta,
        saldoCuenta,
        monedaCuenta,
        fechaCuenta,
        descripcion,
        userId,
        tipocuentaId,
        tipoNombreBancoId
        );
    }

    @Override
    public String toString() {
        return "CuentaCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (numerCuenta != null ? "numerCuenta=" + numerCuenta + ", " : "") +
                (aliasCuenta != null ? "aliasCuenta=" + aliasCuenta + ", " : "") +
                (saldoCuenta != null ? "saldoCuenta=" + saldoCuenta + ", " : "") +
                (monedaCuenta != null ? "monedaCuenta=" + monedaCuenta + ", " : "") +
                (fechaCuenta != null ? "fechaCuenta=" + fechaCuenta + ", " : "") +
                (descripcion != null ? "descripcion=" + descripcion + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
                (tipocuentaId != null ? "tipocuentaId=" + tipocuentaId + ", " : "") +
                (tipoNombreBancoId != null ? "tipoNombreBancoId=" + tipoNombreBancoId + ", " : "") +
            "}";
    }

}
