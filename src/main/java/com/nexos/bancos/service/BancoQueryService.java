package com.nexos.bancos.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.nexos.bancos.domain.Banco;
import com.nexos.bancos.domain.*; // for static metamodels
import com.nexos.bancos.repository.BancoRepository;
import com.nexos.bancos.service.dto.BancoCriteria;

/**
 * Service for executing complex queries for {@link Banco} entities in the database.
 * The main input is a {@link BancoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Banco} or a {@link Page} of {@link Banco} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BancoQueryService extends QueryService<Banco> {

    private final Logger log = LoggerFactory.getLogger(BancoQueryService.class);

    private final BancoRepository bancoRepository;

    public BancoQueryService(BancoRepository bancoRepository) {
        this.bancoRepository = bancoRepository;
    }

    /**
     * Return a {@link List} of {@link Banco} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Banco> findByCriteria(BancoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Banco> specification = createSpecification(criteria);
        return bancoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Banco} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Banco> findByCriteria(BancoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Banco> specification = createSpecification(criteria);
        return bancoRepository.findAll(specification, page);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BancoCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Banco> specification = createSpecification(criteria);
        return bancoRepository.count(specification);
    }

    /**
     * Function to convert BancoCriteria to a {@link Specification}.
     */
    private Specification<Banco> createSpecification(BancoCriteria criteria) {
        Specification<Banco> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Banco_.id));
            }
            if (criteria.getTipoNombreBanco() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTipoNombreBanco(), Banco_.tipoNombreBanco));
            }
        }
        return specification;
    }
}
