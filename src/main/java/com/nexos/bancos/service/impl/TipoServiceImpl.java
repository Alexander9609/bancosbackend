package com.nexos.bancos.service.impl;

import com.nexos.bancos.service.TipoService;
import com.nexos.bancos.domain.Tipo;
import com.nexos.bancos.repository.TipoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Tipo}.
 */
@Service
@Transactional
public class TipoServiceImpl implements TipoService {

    private final Logger log = LoggerFactory.getLogger(TipoServiceImpl.class);

    private final TipoRepository tipoRepository;

    public TipoServiceImpl(TipoRepository tipoRepository) {
        this.tipoRepository = tipoRepository;
    }

    /**
     * Save a tipo.
     *
     * @param tipo the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Tipo save(Tipo tipo) {
        log.debug("Request to save Tipo : {}", tipo);
        return tipoRepository.save(tipo);
    }

    /**
     * Get all the tipos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Tipo> findAll(Pageable pageable) {
        log.debug("Request to get all Tipos");
        return tipoRepository.findAll(pageable);
    }


    /**
     * Get one tipo by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Tipo> findOne(Long id) {
        log.debug("Request to get Tipo : {}", id);
        return tipoRepository.findById(id);
    }

    /**
     * Delete the tipo by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Tipo : {}", id);
        tipoRepository.deleteById(id);
    }
}
