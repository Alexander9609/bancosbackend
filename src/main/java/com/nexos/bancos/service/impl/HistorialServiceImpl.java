package com.nexos.bancos.service.impl;

import com.nexos.bancos.service.HistorialService;
import com.nexos.bancos.domain.Historial;
import com.nexos.bancos.repository.HistorialRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Historial}.
 */
@Service
@Transactional
public class HistorialServiceImpl implements HistorialService {

    private final Logger log = LoggerFactory.getLogger(HistorialServiceImpl.class);

    private final HistorialRepository historialRepository;

    public HistorialServiceImpl(HistorialRepository historialRepository) {
        this.historialRepository = historialRepository;
    }

    /**
     * Save a historial.
     *
     * @param historial the entity to save.
     * @return the persisted entity.
     */
    @Override
    public Historial save(Historial historial) {
        log.debug("Request to save Historial : {}", historial);
        return historialRepository.save(historial);
    }

    /**
     * Get all the historials.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Historial> findAll(Pageable pageable) {
        log.debug("Request to get all Historials");
        return historialRepository.findAll(pageable);
    }


    /**
     * Get one historial by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Historial> findOne(Long id) {
        log.debug("Request to get Historial : {}", id);
        return historialRepository.findById(id);
    }

    /**
     * Delete the historial by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Historial : {}", id);
        historialRepository.deleteById(id);
    }
}
