package com.nexos.bancos.service.impl;

import com.nexos.bancos.service.CuentasAsociadasService;
import com.nexos.bancos.domain.CuentasAsociadas;
import com.nexos.bancos.repository.CuentasAsociadasRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link CuentasAsociadas}.
 */
@Service
@Transactional
public class CuentasAsociadasServiceImpl implements CuentasAsociadasService {

    private final Logger log = LoggerFactory.getLogger(CuentasAsociadasServiceImpl.class);

    private final CuentasAsociadasRepository cuentasAsociadasRepository;

    public CuentasAsociadasServiceImpl(CuentasAsociadasRepository cuentasAsociadasRepository) {
        this.cuentasAsociadasRepository = cuentasAsociadasRepository;
    }

    /**
     * Save a cuentasAsociadas.
     *
     * @param cuentasAsociadas the entity to save.
     * @return the persisted entity.
     */
    @Override
    public CuentasAsociadas save(CuentasAsociadas cuentasAsociadas) {
        log.debug("Request to save CuentasAsociadas : {}", cuentasAsociadas);
        return cuentasAsociadasRepository.save(cuentasAsociadas);
    }

    /**
     * Get all the cuentasAsociadas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<CuentasAsociadas> findAll(Pageable pageable) {
        log.debug("Request to get all CuentasAsociadas");
        return cuentasAsociadasRepository.findAll(pageable);
    }


    /**
     * Get one cuentasAsociadas by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<CuentasAsociadas> findOne(Long id) {
        log.debug("Request to get CuentasAsociadas : {}", id);
        return cuentasAsociadasRepository.findById(id);
    }

    /**
     * Delete the cuentasAsociadas by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete CuentasAsociadas : {}", id);
        cuentasAsociadasRepository.deleteById(id);
    }
}
