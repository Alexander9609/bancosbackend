package com.nexos.bancos.repository;

import com.nexos.bancos.domain.Cuenta;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Cuenta entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CuentaRepository extends JpaRepository<Cuenta, Long>, JpaSpecificationExecutor<Cuenta> {

    @Query("select cuenta from Cuenta cuenta where cuenta.user.login = ?#{principal.username}")
    List<Cuenta> findByUserIsCurrentUser();

}
