package com.nexos.bancos.repository;

import com.nexos.bancos.domain.CuentasAsociadas;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the CuentasAsociadas entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CuentasAsociadasRepository extends JpaRepository<CuentasAsociadas, Long>, JpaSpecificationExecutor<CuentasAsociadas> {

    @Query("select cuentasAsociadas from CuentasAsociadas cuentasAsociadas where cuentasAsociadas.user.login = ?#{principal.username}")
    List<CuentasAsociadas> findByUserIsCurrentUser();

}
