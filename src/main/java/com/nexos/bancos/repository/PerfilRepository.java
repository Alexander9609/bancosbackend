package com.nexos.bancos.repository;

import com.nexos.bancos.domain.Perfil;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Perfil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long>, JpaSpecificationExecutor<Perfil> {

    @Query("select perfil from Perfil perfil where perfil.user.login = ?#{principal.username}")
    List<Perfil> findByUserIsCurrentUser();

}
