package com.nexos.bancos.repository;

import com.nexos.bancos.domain.Tipo;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Tipo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipoRepository extends JpaRepository<Tipo, Long>, JpaSpecificationExecutor<Tipo> {

}
