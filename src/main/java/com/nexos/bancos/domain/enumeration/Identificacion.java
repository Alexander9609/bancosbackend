package com.nexos.bancos.domain.enumeration;

/**
 * The Identificacion enumeration.
 */
public enum Identificacion {
    CEDULA, TARJETA, IDENTIDAD, CEDULA_EXTRANJERIA, PASAPORTE, OTRO
}
