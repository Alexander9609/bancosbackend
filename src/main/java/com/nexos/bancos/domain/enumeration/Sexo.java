package com.nexos.bancos.domain.enumeration;

/**
 * The Sexo enumeration.
 */
public enum Sexo {
    FEMENINO, MASCULINO, OTRO
}
