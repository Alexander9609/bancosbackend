package com.nexos.bancos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A CuentasAsociadas.
 */
@Entity
@Table(name = "cuentas_asociadas")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class CuentasAsociadas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "alias_cuenta_asocia")
    private String aliasCuentaAsocia;

    @ManyToOne
    @JsonIgnoreProperties("cuentasAsociadas")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("cuentasAsociadas")
    private Cuenta cuenta;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAliasCuentaAsocia() {
        return aliasCuentaAsocia;
    }

    public CuentasAsociadas aliasCuentaAsocia(String aliasCuentaAsocia) {
        this.aliasCuentaAsocia = aliasCuentaAsocia;
        return this;
    }

    public void setAliasCuentaAsocia(String aliasCuentaAsocia) {
        this.aliasCuentaAsocia = aliasCuentaAsocia;
    }

    public User getUser() {
        return user;
    }

    public CuentasAsociadas user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public CuentasAsociadas cuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
        return this;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CuentasAsociadas)) {
            return false;
        }
        return id != null && id.equals(((CuentasAsociadas) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "CuentasAsociadas{" +
            "id=" + getId() +
            ", aliasCuentaAsocia='" + getAliasCuentaAsocia() + "'" +
            "}";
    }
}
