package com.nexos.bancos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Historial.
 */
@Entity
@Table(name = "historial")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Historial implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "monto_historial")
    private Integer montoHistorial;

    @Column(name = "descripcion_historial")
    private String descripcionHistorial;

    @Column(name = "id_transaccion")
    private Integer idTransaccion;

    @Column(name = "fecha_historial")
    private Instant fechaHistorial;

    @ManyToOne
    @JsonIgnoreProperties("historials")
    private Cuenta cuenta;

    @ManyToOne
    @JsonIgnoreProperties("historials")
    private CuentasAsociadas cuentasAsociadas;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMontoHistorial() {
        return montoHistorial;
    }

    public Historial montoHistorial(Integer montoHistorial) {
        this.montoHistorial = montoHistorial;
        return this;
    }

    public void setMontoHistorial(Integer montoHistorial) {
        this.montoHistorial = montoHistorial;
    }

    public String getDescripcionHistorial() {
        return descripcionHistorial;
    }

    public Historial descripcionHistorial(String descripcionHistorial) {
        this.descripcionHistorial = descripcionHistorial;
        return this;
    }

    public void setDescripcionHistorial(String descripcionHistorial) {
        this.descripcionHistorial = descripcionHistorial;
    }

    public Integer getIdTransaccion() {
        return idTransaccion;
    }

    public Historial idTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
        return this;
    }

    public void setIdTransaccion(Integer idTransaccion) {
        this.idTransaccion = idTransaccion;
    }

    public Instant getFechaHistorial() {
        return fechaHistorial;
    }

    public Historial fechaHistorial(Instant fechaHistorial) {
        this.fechaHistorial = fechaHistorial;
        return this;
    }

    public void setFechaHistorial(Instant fechaHistorial) {
        this.fechaHistorial = fechaHistorial;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public Historial cuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
        return this;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public CuentasAsociadas getCuentasAsociadas() {
        return cuentasAsociadas;
    }

    public Historial cuentasAsociadas(CuentasAsociadas cuentasAsociadas) {
        this.cuentasAsociadas = cuentasAsociadas;
        return this;
    }

    public void setCuentasAsociadas(CuentasAsociadas cuentasAsociadas) {
        this.cuentasAsociadas = cuentasAsociadas;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Historial)) {
            return false;
        }
        return id != null && id.equals(((Historial) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Historial{" +
            "id=" + getId() +
            ", montoHistorial=" + getMontoHistorial() +
            ", descripcionHistorial='" + getDescripcionHistorial() + "'" +
            ", idTransaccion=" + getIdTransaccion() +
            ", fechaHistorial='" + getFechaHistorial() + "'" +
            "}";
    }
}
