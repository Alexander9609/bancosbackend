package com.nexos.bancos.domain;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Cuenta.
 */
@Entity
@Table(name = "cuenta")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Cuenta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "numer_cuenta")
    private Integer numerCuenta;

    @Column(name = "alias_cuenta")
    private String aliasCuenta;

    @Column(name = "saldo_cuenta")
    private Integer saldoCuenta;

    @Column(name = "moneda_cuenta")
    private String monedaCuenta;

    @Column(name = "fecha_cuenta")
    private Instant fechaCuenta;

    @Column(name = "descripcion")
    private String descripcion;

    @ManyToOne
    @JsonIgnoreProperties("cuentas")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("cuentas")
    private Tipo tipocuenta;

    @ManyToOne
    @JsonIgnoreProperties("cuentas")
    private Banco tipoNombreBanco;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumerCuenta() {
        return numerCuenta;
    }

    public Cuenta numerCuenta(Integer numerCuenta) {
        this.numerCuenta = numerCuenta;
        return this;
    }

    public void setNumerCuenta(Integer numerCuenta) {
        this.numerCuenta = numerCuenta;
    }

    public String getAliasCuenta() {
        return aliasCuenta;
    }

    public Cuenta aliasCuenta(String aliasCuenta) {
        this.aliasCuenta = aliasCuenta;
        return this;
    }

    public void setAliasCuenta(String aliasCuenta) {
        this.aliasCuenta = aliasCuenta;
    }

    public Integer getSaldoCuenta() {
        return saldoCuenta;
    }

    public Cuenta saldoCuenta(Integer saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
        return this;
    }

    public void setSaldoCuenta(Integer saldoCuenta) {
        this.saldoCuenta = saldoCuenta;
    }

    public String getMonedaCuenta() {
        return monedaCuenta;
    }

    public Cuenta monedaCuenta(String monedaCuenta) {
        this.monedaCuenta = monedaCuenta;
        return this;
    }

    public void setMonedaCuenta(String monedaCuenta) {
        this.monedaCuenta = monedaCuenta;
    }

    public Instant getFechaCuenta() {
        return fechaCuenta;
    }

    public Cuenta fechaCuenta(Instant fechaCuenta) {
        this.fechaCuenta = fechaCuenta;
        return this;
    }

    public void setFechaCuenta(Instant fechaCuenta) {
        this.fechaCuenta = fechaCuenta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Cuenta descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public User getUser() {
        return user;
    }

    public Cuenta user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Tipo getTipocuenta() {
        return tipocuenta;
    }

    public Cuenta tipocuenta(Tipo tipo) {
        this.tipocuenta = tipo;
        return this;
    }

    public void setTipocuenta(Tipo tipo) {
        this.tipocuenta = tipo;
    }

    public Banco getTipoNombreBanco() {
        return tipoNombreBanco;
    }

    public Cuenta tipoNombreBanco(Banco banco) {
        this.tipoNombreBanco = banco;
        return this;
    }

    public void setTipoNombreBanco(Banco banco) {
        this.tipoNombreBanco = banco;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cuenta)) {
            return false;
        }
        return id != null && id.equals(((Cuenta) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Cuenta{" +
            "id=" + getId() +
            ", numerCuenta=" + getNumerCuenta() +
            ", aliasCuenta='" + getAliasCuenta() + "'" +
            ", saldoCuenta=" + getSaldoCuenta() +
            ", monedaCuenta='" + getMonedaCuenta() + "'" +
            ", fechaCuenta='" + getFechaCuenta() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            "}";
    }
}
