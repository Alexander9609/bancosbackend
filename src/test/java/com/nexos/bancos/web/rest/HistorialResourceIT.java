package com.nexos.bancos.web.rest;

import com.nexos.bancos.Nexos20App;
import com.nexos.bancos.domain.Historial;
import com.nexos.bancos.domain.Cuenta;
import com.nexos.bancos.domain.CuentasAsociadas;
import com.nexos.bancos.repository.HistorialRepository;
import com.nexos.bancos.service.HistorialService;
import com.nexos.bancos.web.rest.errors.ExceptionTranslator;
import com.nexos.bancos.service.dto.HistorialCriteria;
import com.nexos.bancos.service.HistorialQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nexos.bancos.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link HistorialResource} REST controller.
 */
@SpringBootTest(classes = Nexos20App.class)
public class HistorialResourceIT {

    private static final Integer DEFAULT_MONTO_HISTORIAL = 1;
    private static final Integer UPDATED_MONTO_HISTORIAL = 2;

    private static final String DEFAULT_DESCRIPCION_HISTORIAL = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION_HISTORIAL = "BBBBBBBBBB";

    private static final Integer DEFAULT_ID_TRANSACCION = 1;
    private static final Integer UPDATED_ID_TRANSACCION = 2;

    private static final Instant DEFAULT_FECHA_HISTORIAL = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_HISTORIAL = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private HistorialRepository historialRepository;

    @Autowired
    private HistorialService historialService;

    @Autowired
    private HistorialQueryService historialQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restHistorialMockMvc;

    private Historial historial;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final HistorialResource historialResource = new HistorialResource(historialService, historialQueryService);
        this.restHistorialMockMvc = MockMvcBuilders.standaloneSetup(historialResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Historial createEntity(EntityManager em) {
        Historial historial = new Historial()
            .montoHistorial(DEFAULT_MONTO_HISTORIAL)
            .descripcionHistorial(DEFAULT_DESCRIPCION_HISTORIAL)
            .idTransaccion(DEFAULT_ID_TRANSACCION)
            .fechaHistorial(DEFAULT_FECHA_HISTORIAL);
        return historial;
    }

    @BeforeEach
    public void initTest() {
        historial = createEntity(em);
    }

    @Test
    @Transactional
    public void createHistorial() throws Exception {
        int databaseSizeBeforeCreate = historialRepository.findAll().size();

        // Create the Historial
        restHistorialMockMvc.perform(post("/api/historials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historial)))
            .andExpect(status().isCreated());

        // Validate the Historial in the database
        List<Historial> historialList = historialRepository.findAll();
        assertThat(historialList).hasSize(databaseSizeBeforeCreate + 1);
        Historial testHistorial = historialList.get(historialList.size() - 1);
        assertThat(testHistorial.getMontoHistorial()).isEqualTo(DEFAULT_MONTO_HISTORIAL);
        assertThat(testHistorial.getDescripcionHistorial()).isEqualTo(DEFAULT_DESCRIPCION_HISTORIAL);
        assertThat(testHistorial.getIdTransaccion()).isEqualTo(DEFAULT_ID_TRANSACCION);
        assertThat(testHistorial.getFechaHistorial()).isEqualTo(DEFAULT_FECHA_HISTORIAL);
    }

    @Test
    @Transactional
    public void createHistorialWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = historialRepository.findAll().size();

        // Create the Historial with an existing ID
        historial.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restHistorialMockMvc.perform(post("/api/historials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historial)))
            .andExpect(status().isBadRequest());

        // Validate the Historial in the database
        List<Historial> historialList = historialRepository.findAll();
        assertThat(historialList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllHistorials() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList
        restHistorialMockMvc.perform(get("/api/historials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(historial.getId().intValue())))
            .andExpect(jsonPath("$.[*].montoHistorial").value(hasItem(DEFAULT_MONTO_HISTORIAL)))
            .andExpect(jsonPath("$.[*].descripcionHistorial").value(hasItem(DEFAULT_DESCRIPCION_HISTORIAL.toString())))
            .andExpect(jsonPath("$.[*].idTransaccion").value(hasItem(DEFAULT_ID_TRANSACCION)))
            .andExpect(jsonPath("$.[*].fechaHistorial").value(hasItem(DEFAULT_FECHA_HISTORIAL.toString())));
    }
    
    @Test
    @Transactional
    public void getHistorial() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get the historial
        restHistorialMockMvc.perform(get("/api/historials/{id}", historial.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(historial.getId().intValue()))
            .andExpect(jsonPath("$.montoHistorial").value(DEFAULT_MONTO_HISTORIAL))
            .andExpect(jsonPath("$.descripcionHistorial").value(DEFAULT_DESCRIPCION_HISTORIAL.toString()))
            .andExpect(jsonPath("$.idTransaccion").value(DEFAULT_ID_TRANSACCION))
            .andExpect(jsonPath("$.fechaHistorial").value(DEFAULT_FECHA_HISTORIAL.toString()));
    }

    @Test
    @Transactional
    public void getAllHistorialsByMontoHistorialIsEqualToSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where montoHistorial equals to DEFAULT_MONTO_HISTORIAL
        defaultHistorialShouldBeFound("montoHistorial.equals=" + DEFAULT_MONTO_HISTORIAL);

        // Get all the historialList where montoHistorial equals to UPDATED_MONTO_HISTORIAL
        defaultHistorialShouldNotBeFound("montoHistorial.equals=" + UPDATED_MONTO_HISTORIAL);
    }

    @Test
    @Transactional
    public void getAllHistorialsByMontoHistorialIsInShouldWork() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where montoHistorial in DEFAULT_MONTO_HISTORIAL or UPDATED_MONTO_HISTORIAL
        defaultHistorialShouldBeFound("montoHistorial.in=" + DEFAULT_MONTO_HISTORIAL + "," + UPDATED_MONTO_HISTORIAL);

        // Get all the historialList where montoHistorial equals to UPDATED_MONTO_HISTORIAL
        defaultHistorialShouldNotBeFound("montoHistorial.in=" + UPDATED_MONTO_HISTORIAL);
    }

    @Test
    @Transactional
    public void getAllHistorialsByMontoHistorialIsNullOrNotNull() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where montoHistorial is not null
        defaultHistorialShouldBeFound("montoHistorial.specified=true");

        // Get all the historialList where montoHistorial is null
        defaultHistorialShouldNotBeFound("montoHistorial.specified=false");
    }

    @Test
    @Transactional
    public void getAllHistorialsByMontoHistorialIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where montoHistorial greater than or equals to DEFAULT_MONTO_HISTORIAL
        defaultHistorialShouldBeFound("montoHistorial.greaterOrEqualThan=" + DEFAULT_MONTO_HISTORIAL);

        // Get all the historialList where montoHistorial greater than or equals to UPDATED_MONTO_HISTORIAL
        defaultHistorialShouldNotBeFound("montoHistorial.greaterOrEqualThan=" + UPDATED_MONTO_HISTORIAL);
    }

    @Test
    @Transactional
    public void getAllHistorialsByMontoHistorialIsLessThanSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where montoHistorial less than or equals to DEFAULT_MONTO_HISTORIAL
        defaultHistorialShouldNotBeFound("montoHistorial.lessThan=" + DEFAULT_MONTO_HISTORIAL);

        // Get all the historialList where montoHistorial less than or equals to UPDATED_MONTO_HISTORIAL
        defaultHistorialShouldBeFound("montoHistorial.lessThan=" + UPDATED_MONTO_HISTORIAL);
    }


    @Test
    @Transactional
    public void getAllHistorialsByDescripcionHistorialIsEqualToSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where descripcionHistorial equals to DEFAULT_DESCRIPCION_HISTORIAL
        defaultHistorialShouldBeFound("descripcionHistorial.equals=" + DEFAULT_DESCRIPCION_HISTORIAL);

        // Get all the historialList where descripcionHistorial equals to UPDATED_DESCRIPCION_HISTORIAL
        defaultHistorialShouldNotBeFound("descripcionHistorial.equals=" + UPDATED_DESCRIPCION_HISTORIAL);
    }

    @Test
    @Transactional
    public void getAllHistorialsByDescripcionHistorialIsInShouldWork() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where descripcionHistorial in DEFAULT_DESCRIPCION_HISTORIAL or UPDATED_DESCRIPCION_HISTORIAL
        defaultHistorialShouldBeFound("descripcionHistorial.in=" + DEFAULT_DESCRIPCION_HISTORIAL + "," + UPDATED_DESCRIPCION_HISTORIAL);

        // Get all the historialList where descripcionHistorial equals to UPDATED_DESCRIPCION_HISTORIAL
        defaultHistorialShouldNotBeFound("descripcionHistorial.in=" + UPDATED_DESCRIPCION_HISTORIAL);
    }

    @Test
    @Transactional
    public void getAllHistorialsByDescripcionHistorialIsNullOrNotNull() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where descripcionHistorial is not null
        defaultHistorialShouldBeFound("descripcionHistorial.specified=true");

        // Get all the historialList where descripcionHistorial is null
        defaultHistorialShouldNotBeFound("descripcionHistorial.specified=false");
    }

    @Test
    @Transactional
    public void getAllHistorialsByIdTransaccionIsEqualToSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where idTransaccion equals to DEFAULT_ID_TRANSACCION
        defaultHistorialShouldBeFound("idTransaccion.equals=" + DEFAULT_ID_TRANSACCION);

        // Get all the historialList where idTransaccion equals to UPDATED_ID_TRANSACCION
        defaultHistorialShouldNotBeFound("idTransaccion.equals=" + UPDATED_ID_TRANSACCION);
    }

    @Test
    @Transactional
    public void getAllHistorialsByIdTransaccionIsInShouldWork() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where idTransaccion in DEFAULT_ID_TRANSACCION or UPDATED_ID_TRANSACCION
        defaultHistorialShouldBeFound("idTransaccion.in=" + DEFAULT_ID_TRANSACCION + "," + UPDATED_ID_TRANSACCION);

        // Get all the historialList where idTransaccion equals to UPDATED_ID_TRANSACCION
        defaultHistorialShouldNotBeFound("idTransaccion.in=" + UPDATED_ID_TRANSACCION);
    }

    @Test
    @Transactional
    public void getAllHistorialsByIdTransaccionIsNullOrNotNull() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where idTransaccion is not null
        defaultHistorialShouldBeFound("idTransaccion.specified=true");

        // Get all the historialList where idTransaccion is null
        defaultHistorialShouldNotBeFound("idTransaccion.specified=false");
    }

    @Test
    @Transactional
    public void getAllHistorialsByIdTransaccionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where idTransaccion greater than or equals to DEFAULT_ID_TRANSACCION
        defaultHistorialShouldBeFound("idTransaccion.greaterOrEqualThan=" + DEFAULT_ID_TRANSACCION);

        // Get all the historialList where idTransaccion greater than or equals to UPDATED_ID_TRANSACCION
        defaultHistorialShouldNotBeFound("idTransaccion.greaterOrEqualThan=" + UPDATED_ID_TRANSACCION);
    }

    @Test
    @Transactional
    public void getAllHistorialsByIdTransaccionIsLessThanSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where idTransaccion less than or equals to DEFAULT_ID_TRANSACCION
        defaultHistorialShouldNotBeFound("idTransaccion.lessThan=" + DEFAULT_ID_TRANSACCION);

        // Get all the historialList where idTransaccion less than or equals to UPDATED_ID_TRANSACCION
        defaultHistorialShouldBeFound("idTransaccion.lessThan=" + UPDATED_ID_TRANSACCION);
    }


    @Test
    @Transactional
    public void getAllHistorialsByFechaHistorialIsEqualToSomething() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where fechaHistorial equals to DEFAULT_FECHA_HISTORIAL
        defaultHistorialShouldBeFound("fechaHistorial.equals=" + DEFAULT_FECHA_HISTORIAL);

        // Get all the historialList where fechaHistorial equals to UPDATED_FECHA_HISTORIAL
        defaultHistorialShouldNotBeFound("fechaHistorial.equals=" + UPDATED_FECHA_HISTORIAL);
    }

    @Test
    @Transactional
    public void getAllHistorialsByFechaHistorialIsInShouldWork() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where fechaHistorial in DEFAULT_FECHA_HISTORIAL or UPDATED_FECHA_HISTORIAL
        defaultHistorialShouldBeFound("fechaHistorial.in=" + DEFAULT_FECHA_HISTORIAL + "," + UPDATED_FECHA_HISTORIAL);

        // Get all the historialList where fechaHistorial equals to UPDATED_FECHA_HISTORIAL
        defaultHistorialShouldNotBeFound("fechaHistorial.in=" + UPDATED_FECHA_HISTORIAL);
    }

    @Test
    @Transactional
    public void getAllHistorialsByFechaHistorialIsNullOrNotNull() throws Exception {
        // Initialize the database
        historialRepository.saveAndFlush(historial);

        // Get all the historialList where fechaHistorial is not null
        defaultHistorialShouldBeFound("fechaHistorial.specified=true");

        // Get all the historialList where fechaHistorial is null
        defaultHistorialShouldNotBeFound("fechaHistorial.specified=false");
    }

    @Test
    @Transactional
    public void getAllHistorialsByCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        Cuenta cuenta = CuentaResourceIT.createEntity(em);
        em.persist(cuenta);
        em.flush();
        historial.setCuenta(cuenta);
        historialRepository.saveAndFlush(historial);
        Long cuentaId = cuenta.getId();

        // Get all the historialList where cuenta equals to cuentaId
        defaultHistorialShouldBeFound("cuentaId.equals=" + cuentaId);

        // Get all the historialList where cuenta equals to cuentaId + 1
        defaultHistorialShouldNotBeFound("cuentaId.equals=" + (cuentaId + 1));
    }


    @Test
    @Transactional
    public void getAllHistorialsByCuentasAsociadasIsEqualToSomething() throws Exception {
        // Initialize the database
        CuentasAsociadas cuentasAsociadas = CuentasAsociadasResourceIT.createEntity(em);
        em.persist(cuentasAsociadas);
        em.flush();
        historial.setCuentasAsociadas(cuentasAsociadas);
        historialRepository.saveAndFlush(historial);
        Long cuentasAsociadasId = cuentasAsociadas.getId();

        // Get all the historialList where cuentasAsociadas equals to cuentasAsociadasId
        defaultHistorialShouldBeFound("cuentasAsociadasId.equals=" + cuentasAsociadasId);

        // Get all the historialList where cuentasAsociadas equals to cuentasAsociadasId + 1
        defaultHistorialShouldNotBeFound("cuentasAsociadasId.equals=" + (cuentasAsociadasId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultHistorialShouldBeFound(String filter) throws Exception {
        restHistorialMockMvc.perform(get("/api/historials?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(historial.getId().intValue())))
            .andExpect(jsonPath("$.[*].montoHistorial").value(hasItem(DEFAULT_MONTO_HISTORIAL)))
            .andExpect(jsonPath("$.[*].descripcionHistorial").value(hasItem(DEFAULT_DESCRIPCION_HISTORIAL)))
            .andExpect(jsonPath("$.[*].idTransaccion").value(hasItem(DEFAULT_ID_TRANSACCION)))
            .andExpect(jsonPath("$.[*].fechaHistorial").value(hasItem(DEFAULT_FECHA_HISTORIAL.toString())));

        // Check, that the count call also returns 1
        restHistorialMockMvc.perform(get("/api/historials/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultHistorialShouldNotBeFound(String filter) throws Exception {
        restHistorialMockMvc.perform(get("/api/historials?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restHistorialMockMvc.perform(get("/api/historials/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingHistorial() throws Exception {
        // Get the historial
        restHistorialMockMvc.perform(get("/api/historials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateHistorial() throws Exception {
        // Initialize the database
        historialService.save(historial);

        int databaseSizeBeforeUpdate = historialRepository.findAll().size();

        // Update the historial
        Historial updatedHistorial = historialRepository.findById(historial.getId()).get();
        // Disconnect from session so that the updates on updatedHistorial are not directly saved in db
        em.detach(updatedHistorial);
        updatedHistorial
            .montoHistorial(UPDATED_MONTO_HISTORIAL)
            .descripcionHistorial(UPDATED_DESCRIPCION_HISTORIAL)
            .idTransaccion(UPDATED_ID_TRANSACCION)
            .fechaHistorial(UPDATED_FECHA_HISTORIAL);

        restHistorialMockMvc.perform(put("/api/historials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedHistorial)))
            .andExpect(status().isOk());

        // Validate the Historial in the database
        List<Historial> historialList = historialRepository.findAll();
        assertThat(historialList).hasSize(databaseSizeBeforeUpdate);
        Historial testHistorial = historialList.get(historialList.size() - 1);
        assertThat(testHistorial.getMontoHistorial()).isEqualTo(UPDATED_MONTO_HISTORIAL);
        assertThat(testHistorial.getDescripcionHistorial()).isEqualTo(UPDATED_DESCRIPCION_HISTORIAL);
        assertThat(testHistorial.getIdTransaccion()).isEqualTo(UPDATED_ID_TRANSACCION);
        assertThat(testHistorial.getFechaHistorial()).isEqualTo(UPDATED_FECHA_HISTORIAL);
    }

    @Test
    @Transactional
    public void updateNonExistingHistorial() throws Exception {
        int databaseSizeBeforeUpdate = historialRepository.findAll().size();

        // Create the Historial

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHistorialMockMvc.perform(put("/api/historials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(historial)))
            .andExpect(status().isBadRequest());

        // Validate the Historial in the database
        List<Historial> historialList = historialRepository.findAll();
        assertThat(historialList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteHistorial() throws Exception {
        // Initialize the database
        historialService.save(historial);

        int databaseSizeBeforeDelete = historialRepository.findAll().size();

        // Delete the historial
        restHistorialMockMvc.perform(delete("/api/historials/{id}", historial.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Historial> historialList = historialRepository.findAll();
        assertThat(historialList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Historial.class);
        Historial historial1 = new Historial();
        historial1.setId(1L);
        Historial historial2 = new Historial();
        historial2.setId(historial1.getId());
        assertThat(historial1).isEqualTo(historial2);
        historial2.setId(2L);
        assertThat(historial1).isNotEqualTo(historial2);
        historial1.setId(null);
        assertThat(historial1).isNotEqualTo(historial2);
    }
}
