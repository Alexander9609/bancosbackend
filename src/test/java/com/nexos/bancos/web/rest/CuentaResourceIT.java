package com.nexos.bancos.web.rest;

import com.nexos.bancos.Nexos20App;
import com.nexos.bancos.domain.Cuenta;
import com.nexos.bancos.domain.User;
import com.nexos.bancos.domain.Tipo;
import com.nexos.bancos.domain.Banco;
import com.nexos.bancos.repository.CuentaRepository;
import com.nexos.bancos.service.CuentaService;
import com.nexos.bancos.web.rest.errors.ExceptionTranslator;
import com.nexos.bancos.service.dto.CuentaCriteria;
import com.nexos.bancos.service.CuentaQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nexos.bancos.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CuentaResource} REST controller.
 */
@SpringBootTest(classes = Nexos20App.class)
public class CuentaResourceIT {

    private static final Integer DEFAULT_NUMER_CUENTA = 1;
    private static final Integer UPDATED_NUMER_CUENTA = 2;

    private static final String DEFAULT_ALIAS_CUENTA = "AAAAAAAAAA";
    private static final String UPDATED_ALIAS_CUENTA = "BBBBBBBBBB";

    private static final Integer DEFAULT_SALDO_CUENTA = 1;
    private static final Integer UPDATED_SALDO_CUENTA = 2;

    private static final String DEFAULT_MONEDA_CUENTA = "AAAAAAAAAA";
    private static final String UPDATED_MONEDA_CUENTA = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_CUENTA = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_CUENTA = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    @Autowired
    private CuentaRepository cuentaRepository;

    @Autowired
    private CuentaService cuentaService;

    @Autowired
    private CuentaQueryService cuentaQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCuentaMockMvc;

    private Cuenta cuenta;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CuentaResource cuentaResource = new CuentaResource(cuentaService, cuentaQueryService);
        this.restCuentaMockMvc = MockMvcBuilders.standaloneSetup(cuentaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Cuenta createEntity(EntityManager em) {
        Cuenta cuenta = new Cuenta()
            .numerCuenta(DEFAULT_NUMER_CUENTA)
            .aliasCuenta(DEFAULT_ALIAS_CUENTA)
            .saldoCuenta(DEFAULT_SALDO_CUENTA)
            .monedaCuenta(DEFAULT_MONEDA_CUENTA)
            .fechaCuenta(DEFAULT_FECHA_CUENTA)
            .descripcion(DEFAULT_DESCRIPCION);
        return cuenta;
    }

    @BeforeEach
    public void initTest() {
        cuenta = createEntity(em);
    }

    @Test
    @Transactional
    public void createCuenta() throws Exception {
        int databaseSizeBeforeCreate = cuentaRepository.findAll().size();

        // Create the Cuenta
        restCuentaMockMvc.perform(post("/api/cuentas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuenta)))
            .andExpect(status().isCreated());

        // Validate the Cuenta in the database
        List<Cuenta> cuentaList = cuentaRepository.findAll();
        assertThat(cuentaList).hasSize(databaseSizeBeforeCreate + 1);
        Cuenta testCuenta = cuentaList.get(cuentaList.size() - 1);
        assertThat(testCuenta.getNumerCuenta()).isEqualTo(DEFAULT_NUMER_CUENTA);
        assertThat(testCuenta.getAliasCuenta()).isEqualTo(DEFAULT_ALIAS_CUENTA);
        assertThat(testCuenta.getSaldoCuenta()).isEqualTo(DEFAULT_SALDO_CUENTA);
        assertThat(testCuenta.getMonedaCuenta()).isEqualTo(DEFAULT_MONEDA_CUENTA);
        assertThat(testCuenta.getFechaCuenta()).isEqualTo(DEFAULT_FECHA_CUENTA);
        assertThat(testCuenta.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
    }

    @Test
    @Transactional
    public void createCuentaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cuentaRepository.findAll().size();

        // Create the Cuenta with an existing ID
        cuenta.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCuentaMockMvc.perform(post("/api/cuentas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuenta)))
            .andExpect(status().isBadRequest());

        // Validate the Cuenta in the database
        List<Cuenta> cuentaList = cuentaRepository.findAll();
        assertThat(cuentaList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCuentas() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList
        restCuentaMockMvc.perform(get("/api/cuentas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cuenta.getId().intValue())))
            .andExpect(jsonPath("$.[*].numerCuenta").value(hasItem(DEFAULT_NUMER_CUENTA)))
            .andExpect(jsonPath("$.[*].aliasCuenta").value(hasItem(DEFAULT_ALIAS_CUENTA.toString())))
            .andExpect(jsonPath("$.[*].saldoCuenta").value(hasItem(DEFAULT_SALDO_CUENTA)))
            .andExpect(jsonPath("$.[*].monedaCuenta").value(hasItem(DEFAULT_MONEDA_CUENTA.toString())))
            .andExpect(jsonPath("$.[*].fechaCuenta").value(hasItem(DEFAULT_FECHA_CUENTA.toString())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())));
    }
    
    @Test
    @Transactional
    public void getCuenta() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get the cuenta
        restCuentaMockMvc.perform(get("/api/cuentas/{id}", cuenta.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cuenta.getId().intValue()))
            .andExpect(jsonPath("$.numerCuenta").value(DEFAULT_NUMER_CUENTA))
            .andExpect(jsonPath("$.aliasCuenta").value(DEFAULT_ALIAS_CUENTA.toString()))
            .andExpect(jsonPath("$.saldoCuenta").value(DEFAULT_SALDO_CUENTA))
            .andExpect(jsonPath("$.monedaCuenta").value(DEFAULT_MONEDA_CUENTA.toString()))
            .andExpect(jsonPath("$.fechaCuenta").value(DEFAULT_FECHA_CUENTA.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()));
    }

    @Test
    @Transactional
    public void getAllCuentasByNumerCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where numerCuenta equals to DEFAULT_NUMER_CUENTA
        defaultCuentaShouldBeFound("numerCuenta.equals=" + DEFAULT_NUMER_CUENTA);

        // Get all the cuentaList where numerCuenta equals to UPDATED_NUMER_CUENTA
        defaultCuentaShouldNotBeFound("numerCuenta.equals=" + UPDATED_NUMER_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByNumerCuentaIsInShouldWork() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where numerCuenta in DEFAULT_NUMER_CUENTA or UPDATED_NUMER_CUENTA
        defaultCuentaShouldBeFound("numerCuenta.in=" + DEFAULT_NUMER_CUENTA + "," + UPDATED_NUMER_CUENTA);

        // Get all the cuentaList where numerCuenta equals to UPDATED_NUMER_CUENTA
        defaultCuentaShouldNotBeFound("numerCuenta.in=" + UPDATED_NUMER_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByNumerCuentaIsNullOrNotNull() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where numerCuenta is not null
        defaultCuentaShouldBeFound("numerCuenta.specified=true");

        // Get all the cuentaList where numerCuenta is null
        defaultCuentaShouldNotBeFound("numerCuenta.specified=false");
    }

    @Test
    @Transactional
    public void getAllCuentasByNumerCuentaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where numerCuenta greater than or equals to DEFAULT_NUMER_CUENTA
        defaultCuentaShouldBeFound("numerCuenta.greaterOrEqualThan=" + DEFAULT_NUMER_CUENTA);

        // Get all the cuentaList where numerCuenta greater than or equals to UPDATED_NUMER_CUENTA
        defaultCuentaShouldNotBeFound("numerCuenta.greaterOrEqualThan=" + UPDATED_NUMER_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByNumerCuentaIsLessThanSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where numerCuenta less than or equals to DEFAULT_NUMER_CUENTA
        defaultCuentaShouldNotBeFound("numerCuenta.lessThan=" + DEFAULT_NUMER_CUENTA);

        // Get all the cuentaList where numerCuenta less than or equals to UPDATED_NUMER_CUENTA
        defaultCuentaShouldBeFound("numerCuenta.lessThan=" + UPDATED_NUMER_CUENTA);
    }


    @Test
    @Transactional
    public void getAllCuentasByAliasCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where aliasCuenta equals to DEFAULT_ALIAS_CUENTA
        defaultCuentaShouldBeFound("aliasCuenta.equals=" + DEFAULT_ALIAS_CUENTA);

        // Get all the cuentaList where aliasCuenta equals to UPDATED_ALIAS_CUENTA
        defaultCuentaShouldNotBeFound("aliasCuenta.equals=" + UPDATED_ALIAS_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByAliasCuentaIsInShouldWork() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where aliasCuenta in DEFAULT_ALIAS_CUENTA or UPDATED_ALIAS_CUENTA
        defaultCuentaShouldBeFound("aliasCuenta.in=" + DEFAULT_ALIAS_CUENTA + "," + UPDATED_ALIAS_CUENTA);

        // Get all the cuentaList where aliasCuenta equals to UPDATED_ALIAS_CUENTA
        defaultCuentaShouldNotBeFound("aliasCuenta.in=" + UPDATED_ALIAS_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByAliasCuentaIsNullOrNotNull() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where aliasCuenta is not null
        defaultCuentaShouldBeFound("aliasCuenta.specified=true");

        // Get all the cuentaList where aliasCuenta is null
        defaultCuentaShouldNotBeFound("aliasCuenta.specified=false");
    }

    @Test
    @Transactional
    public void getAllCuentasBySaldoCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where saldoCuenta equals to DEFAULT_SALDO_CUENTA
        defaultCuentaShouldBeFound("saldoCuenta.equals=" + DEFAULT_SALDO_CUENTA);

        // Get all the cuentaList where saldoCuenta equals to UPDATED_SALDO_CUENTA
        defaultCuentaShouldNotBeFound("saldoCuenta.equals=" + UPDATED_SALDO_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasBySaldoCuentaIsInShouldWork() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where saldoCuenta in DEFAULT_SALDO_CUENTA or UPDATED_SALDO_CUENTA
        defaultCuentaShouldBeFound("saldoCuenta.in=" + DEFAULT_SALDO_CUENTA + "," + UPDATED_SALDO_CUENTA);

        // Get all the cuentaList where saldoCuenta equals to UPDATED_SALDO_CUENTA
        defaultCuentaShouldNotBeFound("saldoCuenta.in=" + UPDATED_SALDO_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasBySaldoCuentaIsNullOrNotNull() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where saldoCuenta is not null
        defaultCuentaShouldBeFound("saldoCuenta.specified=true");

        // Get all the cuentaList where saldoCuenta is null
        defaultCuentaShouldNotBeFound("saldoCuenta.specified=false");
    }

    @Test
    @Transactional
    public void getAllCuentasBySaldoCuentaIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where saldoCuenta greater than or equals to DEFAULT_SALDO_CUENTA
        defaultCuentaShouldBeFound("saldoCuenta.greaterOrEqualThan=" + DEFAULT_SALDO_CUENTA);

        // Get all the cuentaList where saldoCuenta greater than or equals to UPDATED_SALDO_CUENTA
        defaultCuentaShouldNotBeFound("saldoCuenta.greaterOrEqualThan=" + UPDATED_SALDO_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasBySaldoCuentaIsLessThanSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where saldoCuenta less than or equals to DEFAULT_SALDO_CUENTA
        defaultCuentaShouldNotBeFound("saldoCuenta.lessThan=" + DEFAULT_SALDO_CUENTA);

        // Get all the cuentaList where saldoCuenta less than or equals to UPDATED_SALDO_CUENTA
        defaultCuentaShouldBeFound("saldoCuenta.lessThan=" + UPDATED_SALDO_CUENTA);
    }


    @Test
    @Transactional
    public void getAllCuentasByMonedaCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where monedaCuenta equals to DEFAULT_MONEDA_CUENTA
        defaultCuentaShouldBeFound("monedaCuenta.equals=" + DEFAULT_MONEDA_CUENTA);

        // Get all the cuentaList where monedaCuenta equals to UPDATED_MONEDA_CUENTA
        defaultCuentaShouldNotBeFound("monedaCuenta.equals=" + UPDATED_MONEDA_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByMonedaCuentaIsInShouldWork() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where monedaCuenta in DEFAULT_MONEDA_CUENTA or UPDATED_MONEDA_CUENTA
        defaultCuentaShouldBeFound("monedaCuenta.in=" + DEFAULT_MONEDA_CUENTA + "," + UPDATED_MONEDA_CUENTA);

        // Get all the cuentaList where monedaCuenta equals to UPDATED_MONEDA_CUENTA
        defaultCuentaShouldNotBeFound("monedaCuenta.in=" + UPDATED_MONEDA_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByMonedaCuentaIsNullOrNotNull() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where monedaCuenta is not null
        defaultCuentaShouldBeFound("monedaCuenta.specified=true");

        // Get all the cuentaList where monedaCuenta is null
        defaultCuentaShouldNotBeFound("monedaCuenta.specified=false");
    }

    @Test
    @Transactional
    public void getAllCuentasByFechaCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where fechaCuenta equals to DEFAULT_FECHA_CUENTA
        defaultCuentaShouldBeFound("fechaCuenta.equals=" + DEFAULT_FECHA_CUENTA);

        // Get all the cuentaList where fechaCuenta equals to UPDATED_FECHA_CUENTA
        defaultCuentaShouldNotBeFound("fechaCuenta.equals=" + UPDATED_FECHA_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByFechaCuentaIsInShouldWork() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where fechaCuenta in DEFAULT_FECHA_CUENTA or UPDATED_FECHA_CUENTA
        defaultCuentaShouldBeFound("fechaCuenta.in=" + DEFAULT_FECHA_CUENTA + "," + UPDATED_FECHA_CUENTA);

        // Get all the cuentaList where fechaCuenta equals to UPDATED_FECHA_CUENTA
        defaultCuentaShouldNotBeFound("fechaCuenta.in=" + UPDATED_FECHA_CUENTA);
    }

    @Test
    @Transactional
    public void getAllCuentasByFechaCuentaIsNullOrNotNull() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where fechaCuenta is not null
        defaultCuentaShouldBeFound("fechaCuenta.specified=true");

        // Get all the cuentaList where fechaCuenta is null
        defaultCuentaShouldNotBeFound("fechaCuenta.specified=false");
    }

    @Test
    @Transactional
    public void getAllCuentasByDescripcionIsEqualToSomething() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where descripcion equals to DEFAULT_DESCRIPCION
        defaultCuentaShouldBeFound("descripcion.equals=" + DEFAULT_DESCRIPCION);

        // Get all the cuentaList where descripcion equals to UPDATED_DESCRIPCION
        defaultCuentaShouldNotBeFound("descripcion.equals=" + UPDATED_DESCRIPCION);
    }

    @Test
    @Transactional
    public void getAllCuentasByDescripcionIsInShouldWork() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where descripcion in DEFAULT_DESCRIPCION or UPDATED_DESCRIPCION
        defaultCuentaShouldBeFound("descripcion.in=" + DEFAULT_DESCRIPCION + "," + UPDATED_DESCRIPCION);

        // Get all the cuentaList where descripcion equals to UPDATED_DESCRIPCION
        defaultCuentaShouldNotBeFound("descripcion.in=" + UPDATED_DESCRIPCION);
    }

    @Test
    @Transactional
    public void getAllCuentasByDescripcionIsNullOrNotNull() throws Exception {
        // Initialize the database
        cuentaRepository.saveAndFlush(cuenta);

        // Get all the cuentaList where descripcion is not null
        defaultCuentaShouldBeFound("descripcion.specified=true");

        // Get all the cuentaList where descripcion is null
        defaultCuentaShouldNotBeFound("descripcion.specified=false");
    }

    @Test
    @Transactional
    public void getAllCuentasByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        cuenta.setUser(user);
        cuentaRepository.saveAndFlush(cuenta);
        Long userId = user.getId();

        // Get all the cuentaList where user equals to userId
        defaultCuentaShouldBeFound("userId.equals=" + userId);

        // Get all the cuentaList where user equals to userId + 1
        defaultCuentaShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllCuentasByTipocuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        Tipo tipocuenta = TipoResourceIT.createEntity(em);
        em.persist(tipocuenta);
        em.flush();
        cuenta.setTipocuenta(tipocuenta);
        cuentaRepository.saveAndFlush(cuenta);
        Long tipocuentaId = tipocuenta.getId();

        // Get all the cuentaList where tipocuenta equals to tipocuentaId
        defaultCuentaShouldBeFound("tipocuentaId.equals=" + tipocuentaId);

        // Get all the cuentaList where tipocuenta equals to tipocuentaId + 1
        defaultCuentaShouldNotBeFound("tipocuentaId.equals=" + (tipocuentaId + 1));
    }


    @Test
    @Transactional
    public void getAllCuentasByTipoNombreBancoIsEqualToSomething() throws Exception {
        // Initialize the database
        Banco tipoNombreBanco = BancoResourceIT.createEntity(em);
        em.persist(tipoNombreBanco);
        em.flush();
        cuenta.setTipoNombreBanco(tipoNombreBanco);
        cuentaRepository.saveAndFlush(cuenta);
        Long tipoNombreBancoId = tipoNombreBanco.getId();

        // Get all the cuentaList where tipoNombreBanco equals to tipoNombreBancoId
        defaultCuentaShouldBeFound("tipoNombreBancoId.equals=" + tipoNombreBancoId);

        // Get all the cuentaList where tipoNombreBanco equals to tipoNombreBancoId + 1
        defaultCuentaShouldNotBeFound("tipoNombreBancoId.equals=" + (tipoNombreBancoId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCuentaShouldBeFound(String filter) throws Exception {
        restCuentaMockMvc.perform(get("/api/cuentas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cuenta.getId().intValue())))
            .andExpect(jsonPath("$.[*].numerCuenta").value(hasItem(DEFAULT_NUMER_CUENTA)))
            .andExpect(jsonPath("$.[*].aliasCuenta").value(hasItem(DEFAULT_ALIAS_CUENTA)))
            .andExpect(jsonPath("$.[*].saldoCuenta").value(hasItem(DEFAULT_SALDO_CUENTA)))
            .andExpect(jsonPath("$.[*].monedaCuenta").value(hasItem(DEFAULT_MONEDA_CUENTA)))
            .andExpect(jsonPath("$.[*].fechaCuenta").value(hasItem(DEFAULT_FECHA_CUENTA.toString())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION)));

        // Check, that the count call also returns 1
        restCuentaMockMvc.perform(get("/api/cuentas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCuentaShouldNotBeFound(String filter) throws Exception {
        restCuentaMockMvc.perform(get("/api/cuentas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCuentaMockMvc.perform(get("/api/cuentas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCuenta() throws Exception {
        // Get the cuenta
        restCuentaMockMvc.perform(get("/api/cuentas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCuenta() throws Exception {
        // Initialize the database
        cuentaService.save(cuenta);

        int databaseSizeBeforeUpdate = cuentaRepository.findAll().size();

        // Update the cuenta
        Cuenta updatedCuenta = cuentaRepository.findById(cuenta.getId()).get();
        // Disconnect from session so that the updates on updatedCuenta are not directly saved in db
        em.detach(updatedCuenta);
        updatedCuenta
            .numerCuenta(UPDATED_NUMER_CUENTA)
            .aliasCuenta(UPDATED_ALIAS_CUENTA)
            .saldoCuenta(UPDATED_SALDO_CUENTA)
            .monedaCuenta(UPDATED_MONEDA_CUENTA)
            .fechaCuenta(UPDATED_FECHA_CUENTA)
            .descripcion(UPDATED_DESCRIPCION);

        restCuentaMockMvc.perform(put("/api/cuentas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCuenta)))
            .andExpect(status().isOk());

        // Validate the Cuenta in the database
        List<Cuenta> cuentaList = cuentaRepository.findAll();
        assertThat(cuentaList).hasSize(databaseSizeBeforeUpdate);
        Cuenta testCuenta = cuentaList.get(cuentaList.size() - 1);
        assertThat(testCuenta.getNumerCuenta()).isEqualTo(UPDATED_NUMER_CUENTA);
        assertThat(testCuenta.getAliasCuenta()).isEqualTo(UPDATED_ALIAS_CUENTA);
        assertThat(testCuenta.getSaldoCuenta()).isEqualTo(UPDATED_SALDO_CUENTA);
        assertThat(testCuenta.getMonedaCuenta()).isEqualTo(UPDATED_MONEDA_CUENTA);
        assertThat(testCuenta.getFechaCuenta()).isEqualTo(UPDATED_FECHA_CUENTA);
        assertThat(testCuenta.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
    }

    @Test
    @Transactional
    public void updateNonExistingCuenta() throws Exception {
        int databaseSizeBeforeUpdate = cuentaRepository.findAll().size();

        // Create the Cuenta

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCuentaMockMvc.perform(put("/api/cuentas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuenta)))
            .andExpect(status().isBadRequest());

        // Validate the Cuenta in the database
        List<Cuenta> cuentaList = cuentaRepository.findAll();
        assertThat(cuentaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCuenta() throws Exception {
        // Initialize the database
        cuentaService.save(cuenta);

        int databaseSizeBeforeDelete = cuentaRepository.findAll().size();

        // Delete the cuenta
        restCuentaMockMvc.perform(delete("/api/cuentas/{id}", cuenta.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Cuenta> cuentaList = cuentaRepository.findAll();
        assertThat(cuentaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Cuenta.class);
        Cuenta cuenta1 = new Cuenta();
        cuenta1.setId(1L);
        Cuenta cuenta2 = new Cuenta();
        cuenta2.setId(cuenta1.getId());
        assertThat(cuenta1).isEqualTo(cuenta2);
        cuenta2.setId(2L);
        assertThat(cuenta1).isNotEqualTo(cuenta2);
        cuenta1.setId(null);
        assertThat(cuenta1).isNotEqualTo(cuenta2);
    }
}
