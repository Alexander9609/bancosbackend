package com.nexos.bancos.web.rest;

import com.nexos.bancos.Nexos20App;
import com.nexos.bancos.domain.CuentasAsociadas;
import com.nexos.bancos.domain.User;
import com.nexos.bancos.domain.Cuenta;
import com.nexos.bancos.repository.CuentasAsociadasRepository;
import com.nexos.bancos.service.CuentasAsociadasService;
import com.nexos.bancos.web.rest.errors.ExceptionTranslator;
import com.nexos.bancos.service.dto.CuentasAsociadasCriteria;
import com.nexos.bancos.service.CuentasAsociadasQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.nexos.bancos.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link CuentasAsociadasResource} REST controller.
 */
@SpringBootTest(classes = Nexos20App.class)
public class CuentasAsociadasResourceIT {

    private static final String DEFAULT_ALIAS_CUENTA_ASOCIA = "AAAAAAAAAA";
    private static final String UPDATED_ALIAS_CUENTA_ASOCIA = "BBBBBBBBBB";

    @Autowired
    private CuentasAsociadasRepository cuentasAsociadasRepository;

    @Autowired
    private CuentasAsociadasService cuentasAsociadasService;

    @Autowired
    private CuentasAsociadasQueryService cuentasAsociadasQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCuentasAsociadasMockMvc;

    private CuentasAsociadas cuentasAsociadas;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CuentasAsociadasResource cuentasAsociadasResource = new CuentasAsociadasResource(cuentasAsociadasService, cuentasAsociadasQueryService);
        this.restCuentasAsociadasMockMvc = MockMvcBuilders.standaloneSetup(cuentasAsociadasResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CuentasAsociadas createEntity(EntityManager em) {
        CuentasAsociadas cuentasAsociadas = new CuentasAsociadas()
            .aliasCuentaAsocia(DEFAULT_ALIAS_CUENTA_ASOCIA);
        return cuentasAsociadas;
    }

    @BeforeEach
    public void initTest() {
        cuentasAsociadas = createEntity(em);
    }

    @Test
    @Transactional
    public void createCuentasAsociadas() throws Exception {
        int databaseSizeBeforeCreate = cuentasAsociadasRepository.findAll().size();

        // Create the CuentasAsociadas
        restCuentasAsociadasMockMvc.perform(post("/api/cuentas-asociadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuentasAsociadas)))
            .andExpect(status().isCreated());

        // Validate the CuentasAsociadas in the database
        List<CuentasAsociadas> cuentasAsociadasList = cuentasAsociadasRepository.findAll();
        assertThat(cuentasAsociadasList).hasSize(databaseSizeBeforeCreate + 1);
        CuentasAsociadas testCuentasAsociadas = cuentasAsociadasList.get(cuentasAsociadasList.size() - 1);
        assertThat(testCuentasAsociadas.getAliasCuentaAsocia()).isEqualTo(DEFAULT_ALIAS_CUENTA_ASOCIA);
    }

    @Test
    @Transactional
    public void createCuentasAsociadasWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = cuentasAsociadasRepository.findAll().size();

        // Create the CuentasAsociadas with an existing ID
        cuentasAsociadas.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCuentasAsociadasMockMvc.perform(post("/api/cuentas-asociadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuentasAsociadas)))
            .andExpect(status().isBadRequest());

        // Validate the CuentasAsociadas in the database
        List<CuentasAsociadas> cuentasAsociadasList = cuentasAsociadasRepository.findAll();
        assertThat(cuentasAsociadasList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllCuentasAsociadas() throws Exception {
        // Initialize the database
        cuentasAsociadasRepository.saveAndFlush(cuentasAsociadas);

        // Get all the cuentasAsociadasList
        restCuentasAsociadasMockMvc.perform(get("/api/cuentas-asociadas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cuentasAsociadas.getId().intValue())))
            .andExpect(jsonPath("$.[*].aliasCuentaAsocia").value(hasItem(DEFAULT_ALIAS_CUENTA_ASOCIA.toString())));
    }
    
    @Test
    @Transactional
    public void getCuentasAsociadas() throws Exception {
        // Initialize the database
        cuentasAsociadasRepository.saveAndFlush(cuentasAsociadas);

        // Get the cuentasAsociadas
        restCuentasAsociadasMockMvc.perform(get("/api/cuentas-asociadas/{id}", cuentasAsociadas.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(cuentasAsociadas.getId().intValue()))
            .andExpect(jsonPath("$.aliasCuentaAsocia").value(DEFAULT_ALIAS_CUENTA_ASOCIA.toString()));
    }

    @Test
    @Transactional
    public void getAllCuentasAsociadasByAliasCuentaAsociaIsEqualToSomething() throws Exception {
        // Initialize the database
        cuentasAsociadasRepository.saveAndFlush(cuentasAsociadas);

        // Get all the cuentasAsociadasList where aliasCuentaAsocia equals to DEFAULT_ALIAS_CUENTA_ASOCIA
        defaultCuentasAsociadasShouldBeFound("aliasCuentaAsocia.equals=" + DEFAULT_ALIAS_CUENTA_ASOCIA);

        // Get all the cuentasAsociadasList where aliasCuentaAsocia equals to UPDATED_ALIAS_CUENTA_ASOCIA
        defaultCuentasAsociadasShouldNotBeFound("aliasCuentaAsocia.equals=" + UPDATED_ALIAS_CUENTA_ASOCIA);
    }

    @Test
    @Transactional
    public void getAllCuentasAsociadasByAliasCuentaAsociaIsInShouldWork() throws Exception {
        // Initialize the database
        cuentasAsociadasRepository.saveAndFlush(cuentasAsociadas);

        // Get all the cuentasAsociadasList where aliasCuentaAsocia in DEFAULT_ALIAS_CUENTA_ASOCIA or UPDATED_ALIAS_CUENTA_ASOCIA
        defaultCuentasAsociadasShouldBeFound("aliasCuentaAsocia.in=" + DEFAULT_ALIAS_CUENTA_ASOCIA + "," + UPDATED_ALIAS_CUENTA_ASOCIA);

        // Get all the cuentasAsociadasList where aliasCuentaAsocia equals to UPDATED_ALIAS_CUENTA_ASOCIA
        defaultCuentasAsociadasShouldNotBeFound("aliasCuentaAsocia.in=" + UPDATED_ALIAS_CUENTA_ASOCIA);
    }

    @Test
    @Transactional
    public void getAllCuentasAsociadasByAliasCuentaAsociaIsNullOrNotNull() throws Exception {
        // Initialize the database
        cuentasAsociadasRepository.saveAndFlush(cuentasAsociadas);

        // Get all the cuentasAsociadasList where aliasCuentaAsocia is not null
        defaultCuentasAsociadasShouldBeFound("aliasCuentaAsocia.specified=true");

        // Get all the cuentasAsociadasList where aliasCuentaAsocia is null
        defaultCuentasAsociadasShouldNotBeFound("aliasCuentaAsocia.specified=false");
    }

    @Test
    @Transactional
    public void getAllCuentasAsociadasByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        cuentasAsociadas.setUser(user);
        cuentasAsociadasRepository.saveAndFlush(cuentasAsociadas);
        Long userId = user.getId();

        // Get all the cuentasAsociadasList where user equals to userId
        defaultCuentasAsociadasShouldBeFound("userId.equals=" + userId);

        // Get all the cuentasAsociadasList where user equals to userId + 1
        defaultCuentasAsociadasShouldNotBeFound("userId.equals=" + (userId + 1));
    }


    @Test
    @Transactional
    public void getAllCuentasAsociadasByCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        Cuenta cuenta = CuentaResourceIT.createEntity(em);
        em.persist(cuenta);
        em.flush();
        cuentasAsociadas.setCuenta(cuenta);
        cuentasAsociadasRepository.saveAndFlush(cuentasAsociadas);
        Long cuentaId = cuenta.getId();

        // Get all the cuentasAsociadasList where cuenta equals to cuentaId
        defaultCuentasAsociadasShouldBeFound("cuentaId.equals=" + cuentaId);

        // Get all the cuentasAsociadasList where cuenta equals to cuentaId + 1
        defaultCuentasAsociadasShouldNotBeFound("cuentaId.equals=" + (cuentaId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultCuentasAsociadasShouldBeFound(String filter) throws Exception {
        restCuentasAsociadasMockMvc.perform(get("/api/cuentas-asociadas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(cuentasAsociadas.getId().intValue())))
            .andExpect(jsonPath("$.[*].aliasCuentaAsocia").value(hasItem(DEFAULT_ALIAS_CUENTA_ASOCIA)));

        // Check, that the count call also returns 1
        restCuentasAsociadasMockMvc.perform(get("/api/cuentas-asociadas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultCuentasAsociadasShouldNotBeFound(String filter) throws Exception {
        restCuentasAsociadasMockMvc.perform(get("/api/cuentas-asociadas?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restCuentasAsociadasMockMvc.perform(get("/api/cuentas-asociadas/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingCuentasAsociadas() throws Exception {
        // Get the cuentasAsociadas
        restCuentasAsociadasMockMvc.perform(get("/api/cuentas-asociadas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCuentasAsociadas() throws Exception {
        // Initialize the database
        cuentasAsociadasService.save(cuentasAsociadas);

        int databaseSizeBeforeUpdate = cuentasAsociadasRepository.findAll().size();

        // Update the cuentasAsociadas
        CuentasAsociadas updatedCuentasAsociadas = cuentasAsociadasRepository.findById(cuentasAsociadas.getId()).get();
        // Disconnect from session so that the updates on updatedCuentasAsociadas are not directly saved in db
        em.detach(updatedCuentasAsociadas);
        updatedCuentasAsociadas
            .aliasCuentaAsocia(UPDATED_ALIAS_CUENTA_ASOCIA);

        restCuentasAsociadasMockMvc.perform(put("/api/cuentas-asociadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCuentasAsociadas)))
            .andExpect(status().isOk());

        // Validate the CuentasAsociadas in the database
        List<CuentasAsociadas> cuentasAsociadasList = cuentasAsociadasRepository.findAll();
        assertThat(cuentasAsociadasList).hasSize(databaseSizeBeforeUpdate);
        CuentasAsociadas testCuentasAsociadas = cuentasAsociadasList.get(cuentasAsociadasList.size() - 1);
        assertThat(testCuentasAsociadas.getAliasCuentaAsocia()).isEqualTo(UPDATED_ALIAS_CUENTA_ASOCIA);
    }

    @Test
    @Transactional
    public void updateNonExistingCuentasAsociadas() throws Exception {
        int databaseSizeBeforeUpdate = cuentasAsociadasRepository.findAll().size();

        // Create the CuentasAsociadas

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCuentasAsociadasMockMvc.perform(put("/api/cuentas-asociadas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(cuentasAsociadas)))
            .andExpect(status().isBadRequest());

        // Validate the CuentasAsociadas in the database
        List<CuentasAsociadas> cuentasAsociadasList = cuentasAsociadasRepository.findAll();
        assertThat(cuentasAsociadasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCuentasAsociadas() throws Exception {
        // Initialize the database
        cuentasAsociadasService.save(cuentasAsociadas);

        int databaseSizeBeforeDelete = cuentasAsociadasRepository.findAll().size();

        // Delete the cuentasAsociadas
        restCuentasAsociadasMockMvc.perform(delete("/api/cuentas-asociadas/{id}", cuentasAsociadas.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<CuentasAsociadas> cuentasAsociadasList = cuentasAsociadasRepository.findAll();
        assertThat(cuentasAsociadasList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CuentasAsociadas.class);
        CuentasAsociadas cuentasAsociadas1 = new CuentasAsociadas();
        cuentasAsociadas1.setId(1L);
        CuentasAsociadas cuentasAsociadas2 = new CuentasAsociadas();
        cuentasAsociadas2.setId(cuentasAsociadas1.getId());
        assertThat(cuentasAsociadas1).isEqualTo(cuentasAsociadas2);
        cuentasAsociadas2.setId(2L);
        assertThat(cuentasAsociadas1).isNotEqualTo(cuentasAsociadas2);
        cuentasAsociadas1.setId(null);
        assertThat(cuentasAsociadas1).isNotEqualTo(cuentasAsociadas2);
    }
}
