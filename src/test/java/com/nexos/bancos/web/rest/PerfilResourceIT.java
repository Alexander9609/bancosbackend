package com.nexos.bancos.web.rest;

import com.nexos.bancos.Nexos20App;
import com.nexos.bancos.domain.Perfil;
import com.nexos.bancos.domain.User;
import com.nexos.bancos.repository.PerfilRepository;
import com.nexos.bancos.service.PerfilService;
import com.nexos.bancos.web.rest.errors.ExceptionTranslator;
import com.nexos.bancos.service.dto.PerfilCriteria;
import com.nexos.bancos.service.PerfilQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.nexos.bancos.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.nexos.bancos.domain.enumeration.Identificacion;
import com.nexos.bancos.domain.enumeration.Sexo;
import com.nexos.bancos.domain.enumeration.Nacionalidad;
/**
 * Integration tests for the {@Link PerfilResource} REST controller.
 */
@SpringBootTest(classes = Nexos20App.class)
public class PerfilResourceIT {

    private static final Identificacion DEFAULT_TIPO_IDENTIFICACION = Identificacion.CEDULA;
    private static final Identificacion UPDATED_TIPO_IDENTIFICACION = Identificacion.TARJETA;

    private static final Integer DEFAULT_IDENTIFICACION = 1;
    private static final Integer UPDATED_IDENTIFICACION = 2;

    private static final Integer DEFAULT_EDAD = 1;
    private static final Integer UPDATED_EDAD = 2;

    private static final Sexo DEFAULT_SEXO = Sexo.FEMENINO;
    private static final Sexo UPDATED_SEXO = Sexo.MASCULINO;

    private static final String DEFAULT_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_DIRECCION = "BBBBBBBBBB";

    private static final Instant DEFAULT_FECHA_NACIMIENTO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_FECHA_NACIMIENTO = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Nacionalidad DEFAULT_NACIONALIDAD = Nacionalidad.Colombia;
    private static final Nacionalidad UPDATED_NACIONALIDAD = Nacionalidad.Colombia;

    private static final Integer DEFAULT_TELEFONO = 1;
    private static final Integer UPDATED_TELEFONO = 2;

    @Autowired
    private PerfilRepository perfilRepository;

    @Autowired
    private PerfilService perfilService;

    @Autowired
    private PerfilQueryService perfilQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPerfilMockMvc;

    private Perfil perfil;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PerfilResource perfilResource = new PerfilResource(perfilService, perfilQueryService);
        this.restPerfilMockMvc = MockMvcBuilders.standaloneSetup(perfilResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Perfil createEntity(EntityManager em) {
        Perfil perfil = new Perfil()
            .tipoIdentificacion(DEFAULT_TIPO_IDENTIFICACION)
            .identificacion(DEFAULT_IDENTIFICACION)
            .edad(DEFAULT_EDAD)
            .sexo(DEFAULT_SEXO)
            .direccion(DEFAULT_DIRECCION)
            .fechaNacimiento(DEFAULT_FECHA_NACIMIENTO)
            .nacionalidad(DEFAULT_NACIONALIDAD)
            .telefono(DEFAULT_TELEFONO);
        return perfil;
    }

    @BeforeEach
    public void initTest() {
        perfil = createEntity(em);
    }

    @Test
    @Transactional
    public void createPerfil() throws Exception {
        int databaseSizeBeforeCreate = perfilRepository.findAll().size();

        // Create the Perfil
        restPerfilMockMvc.perform(post("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isCreated());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeCreate + 1);
        Perfil testPerfil = perfilList.get(perfilList.size() - 1);
        assertThat(testPerfil.getTipoIdentificacion()).isEqualTo(DEFAULT_TIPO_IDENTIFICACION);
        assertThat(testPerfil.getIdentificacion()).isEqualTo(DEFAULT_IDENTIFICACION);
        assertThat(testPerfil.getEdad()).isEqualTo(DEFAULT_EDAD);
        assertThat(testPerfil.getSexo()).isEqualTo(DEFAULT_SEXO);
        assertThat(testPerfil.getDireccion()).isEqualTo(DEFAULT_DIRECCION);
        assertThat(testPerfil.getFechaNacimiento()).isEqualTo(DEFAULT_FECHA_NACIMIENTO);
        assertThat(testPerfil.getNacionalidad()).isEqualTo(DEFAULT_NACIONALIDAD);
        assertThat(testPerfil.getTelefono()).isEqualTo(DEFAULT_TELEFONO);
    }

    @Test
    @Transactional
    public void createPerfilWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = perfilRepository.findAll().size();

        // Create the Perfil with an existing ID
        perfil.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPerfilMockMvc.perform(post("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isBadRequest());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPerfils() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList
        restPerfilMockMvc.perform(get("/api/perfils?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perfil.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoIdentificacion").value(hasItem(DEFAULT_TIPO_IDENTIFICACION.toString())))
            .andExpect(jsonPath("$.[*].identificacion").value(hasItem(DEFAULT_IDENTIFICACION)))
            .andExpect(jsonPath("$.[*].edad").value(hasItem(DEFAULT_EDAD)))
            .andExpect(jsonPath("$.[*].sexo").value(hasItem(DEFAULT_SEXO.toString())))
            .andExpect(jsonPath("$.[*].direccion").value(hasItem(DEFAULT_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].fechaNacimiento").value(hasItem(DEFAULT_FECHA_NACIMIENTO.toString())))
            .andExpect(jsonPath("$.[*].nacionalidad").value(hasItem(DEFAULT_NACIONALIDAD.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)));
    }
    
    @Test
    @Transactional
    public void getPerfil() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get the perfil
        restPerfilMockMvc.perform(get("/api/perfils/{id}", perfil.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(perfil.getId().intValue()))
            .andExpect(jsonPath("$.tipoIdentificacion").value(DEFAULT_TIPO_IDENTIFICACION.toString()))
            .andExpect(jsonPath("$.identificacion").value(DEFAULT_IDENTIFICACION))
            .andExpect(jsonPath("$.edad").value(DEFAULT_EDAD))
            .andExpect(jsonPath("$.sexo").value(DEFAULT_SEXO.toString()))
            .andExpect(jsonPath("$.direccion").value(DEFAULT_DIRECCION.toString()))
            .andExpect(jsonPath("$.fechaNacimiento").value(DEFAULT_FECHA_NACIMIENTO.toString()))
            .andExpect(jsonPath("$.nacionalidad").value(DEFAULT_NACIONALIDAD.toString()))
            .andExpect(jsonPath("$.telefono").value(DEFAULT_TELEFONO));
    }

    @Test
    @Transactional
    public void getAllPerfilsByTipoIdentificacionIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where tipoIdentificacion equals to DEFAULT_TIPO_IDENTIFICACION
        defaultPerfilShouldBeFound("tipoIdentificacion.equals=" + DEFAULT_TIPO_IDENTIFICACION);

        // Get all the perfilList where tipoIdentificacion equals to UPDATED_TIPO_IDENTIFICACION
        defaultPerfilShouldNotBeFound("tipoIdentificacion.equals=" + UPDATED_TIPO_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllPerfilsByTipoIdentificacionIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where tipoIdentificacion in DEFAULT_TIPO_IDENTIFICACION or UPDATED_TIPO_IDENTIFICACION
        defaultPerfilShouldBeFound("tipoIdentificacion.in=" + DEFAULT_TIPO_IDENTIFICACION + "," + UPDATED_TIPO_IDENTIFICACION);

        // Get all the perfilList where tipoIdentificacion equals to UPDATED_TIPO_IDENTIFICACION
        defaultPerfilShouldNotBeFound("tipoIdentificacion.in=" + UPDATED_TIPO_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllPerfilsByTipoIdentificacionIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where tipoIdentificacion is not null
        defaultPerfilShouldBeFound("tipoIdentificacion.specified=true");

        // Get all the perfilList where tipoIdentificacion is null
        defaultPerfilShouldNotBeFound("tipoIdentificacion.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByIdentificacionIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where identificacion equals to DEFAULT_IDENTIFICACION
        defaultPerfilShouldBeFound("identificacion.equals=" + DEFAULT_IDENTIFICACION);

        // Get all the perfilList where identificacion equals to UPDATED_IDENTIFICACION
        defaultPerfilShouldNotBeFound("identificacion.equals=" + UPDATED_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllPerfilsByIdentificacionIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where identificacion in DEFAULT_IDENTIFICACION or UPDATED_IDENTIFICACION
        defaultPerfilShouldBeFound("identificacion.in=" + DEFAULT_IDENTIFICACION + "," + UPDATED_IDENTIFICACION);

        // Get all the perfilList where identificacion equals to UPDATED_IDENTIFICACION
        defaultPerfilShouldNotBeFound("identificacion.in=" + UPDATED_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllPerfilsByIdentificacionIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where identificacion is not null
        defaultPerfilShouldBeFound("identificacion.specified=true");

        // Get all the perfilList where identificacion is null
        defaultPerfilShouldNotBeFound("identificacion.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByIdentificacionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where identificacion greater than or equals to DEFAULT_IDENTIFICACION
        defaultPerfilShouldBeFound("identificacion.greaterOrEqualThan=" + DEFAULT_IDENTIFICACION);

        // Get all the perfilList where identificacion greater than or equals to UPDATED_IDENTIFICACION
        defaultPerfilShouldNotBeFound("identificacion.greaterOrEqualThan=" + UPDATED_IDENTIFICACION);
    }

    @Test
    @Transactional
    public void getAllPerfilsByIdentificacionIsLessThanSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where identificacion less than or equals to DEFAULT_IDENTIFICACION
        defaultPerfilShouldNotBeFound("identificacion.lessThan=" + DEFAULT_IDENTIFICACION);

        // Get all the perfilList where identificacion less than or equals to UPDATED_IDENTIFICACION
        defaultPerfilShouldBeFound("identificacion.lessThan=" + UPDATED_IDENTIFICACION);
    }


    @Test
    @Transactional
    public void getAllPerfilsByEdadIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where edad equals to DEFAULT_EDAD
        defaultPerfilShouldBeFound("edad.equals=" + DEFAULT_EDAD);

        // Get all the perfilList where edad equals to UPDATED_EDAD
        defaultPerfilShouldNotBeFound("edad.equals=" + UPDATED_EDAD);
    }

    @Test
    @Transactional
    public void getAllPerfilsByEdadIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where edad in DEFAULT_EDAD or UPDATED_EDAD
        defaultPerfilShouldBeFound("edad.in=" + DEFAULT_EDAD + "," + UPDATED_EDAD);

        // Get all the perfilList where edad equals to UPDATED_EDAD
        defaultPerfilShouldNotBeFound("edad.in=" + UPDATED_EDAD);
    }

    @Test
    @Transactional
    public void getAllPerfilsByEdadIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where edad is not null
        defaultPerfilShouldBeFound("edad.specified=true");

        // Get all the perfilList where edad is null
        defaultPerfilShouldNotBeFound("edad.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByEdadIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where edad greater than or equals to DEFAULT_EDAD
        defaultPerfilShouldBeFound("edad.greaterOrEqualThan=" + DEFAULT_EDAD);

        // Get all the perfilList where edad greater than or equals to UPDATED_EDAD
        defaultPerfilShouldNotBeFound("edad.greaterOrEqualThan=" + UPDATED_EDAD);
    }

    @Test
    @Transactional
    public void getAllPerfilsByEdadIsLessThanSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where edad less than or equals to DEFAULT_EDAD
        defaultPerfilShouldNotBeFound("edad.lessThan=" + DEFAULT_EDAD);

        // Get all the perfilList where edad less than or equals to UPDATED_EDAD
        defaultPerfilShouldBeFound("edad.lessThan=" + UPDATED_EDAD);
    }


    @Test
    @Transactional
    public void getAllPerfilsBySexoIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where sexo equals to DEFAULT_SEXO
        defaultPerfilShouldBeFound("sexo.equals=" + DEFAULT_SEXO);

        // Get all the perfilList where sexo equals to UPDATED_SEXO
        defaultPerfilShouldNotBeFound("sexo.equals=" + UPDATED_SEXO);
    }

    @Test
    @Transactional
    public void getAllPerfilsBySexoIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where sexo in DEFAULT_SEXO or UPDATED_SEXO
        defaultPerfilShouldBeFound("sexo.in=" + DEFAULT_SEXO + "," + UPDATED_SEXO);

        // Get all the perfilList where sexo equals to UPDATED_SEXO
        defaultPerfilShouldNotBeFound("sexo.in=" + UPDATED_SEXO);
    }

    @Test
    @Transactional
    public void getAllPerfilsBySexoIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where sexo is not null
        defaultPerfilShouldBeFound("sexo.specified=true");

        // Get all the perfilList where sexo is null
        defaultPerfilShouldNotBeFound("sexo.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByDireccionIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where direccion equals to DEFAULT_DIRECCION
        defaultPerfilShouldBeFound("direccion.equals=" + DEFAULT_DIRECCION);

        // Get all the perfilList where direccion equals to UPDATED_DIRECCION
        defaultPerfilShouldNotBeFound("direccion.equals=" + UPDATED_DIRECCION);
    }

    @Test
    @Transactional
    public void getAllPerfilsByDireccionIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where direccion in DEFAULT_DIRECCION or UPDATED_DIRECCION
        defaultPerfilShouldBeFound("direccion.in=" + DEFAULT_DIRECCION + "," + UPDATED_DIRECCION);

        // Get all the perfilList where direccion equals to UPDATED_DIRECCION
        defaultPerfilShouldNotBeFound("direccion.in=" + UPDATED_DIRECCION);
    }

    @Test
    @Transactional
    public void getAllPerfilsByDireccionIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where direccion is not null
        defaultPerfilShouldBeFound("direccion.specified=true");

        // Get all the perfilList where direccion is null
        defaultPerfilShouldNotBeFound("direccion.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByFechaNacimientoIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where fechaNacimiento equals to DEFAULT_FECHA_NACIMIENTO
        defaultPerfilShouldBeFound("fechaNacimiento.equals=" + DEFAULT_FECHA_NACIMIENTO);

        // Get all the perfilList where fechaNacimiento equals to UPDATED_FECHA_NACIMIENTO
        defaultPerfilShouldNotBeFound("fechaNacimiento.equals=" + UPDATED_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllPerfilsByFechaNacimientoIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where fechaNacimiento in DEFAULT_FECHA_NACIMIENTO or UPDATED_FECHA_NACIMIENTO
        defaultPerfilShouldBeFound("fechaNacimiento.in=" + DEFAULT_FECHA_NACIMIENTO + "," + UPDATED_FECHA_NACIMIENTO);

        // Get all the perfilList where fechaNacimiento equals to UPDATED_FECHA_NACIMIENTO
        defaultPerfilShouldNotBeFound("fechaNacimiento.in=" + UPDATED_FECHA_NACIMIENTO);
    }

    @Test
    @Transactional
    public void getAllPerfilsByFechaNacimientoIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where fechaNacimiento is not null
        defaultPerfilShouldBeFound("fechaNacimiento.specified=true");

        // Get all the perfilList where fechaNacimiento is null
        defaultPerfilShouldNotBeFound("fechaNacimiento.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByNacionalidadIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where nacionalidad equals to DEFAULT_NACIONALIDAD
        defaultPerfilShouldBeFound("nacionalidad.equals=" + DEFAULT_NACIONALIDAD);

        // Get all the perfilList where nacionalidad equals to UPDATED_NACIONALIDAD
        defaultPerfilShouldNotBeFound("nacionalidad.equals=" + UPDATED_NACIONALIDAD);
    }

    @Test
    @Transactional
    public void getAllPerfilsByNacionalidadIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where nacionalidad in DEFAULT_NACIONALIDAD or UPDATED_NACIONALIDAD
        defaultPerfilShouldBeFound("nacionalidad.in=" + DEFAULT_NACIONALIDAD + "," + UPDATED_NACIONALIDAD);

        // Get all the perfilList where nacionalidad equals to UPDATED_NACIONALIDAD
        defaultPerfilShouldNotBeFound("nacionalidad.in=" + UPDATED_NACIONALIDAD);
    }

    @Test
    @Transactional
    public void getAllPerfilsByNacionalidadIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where nacionalidad is not null
        defaultPerfilShouldBeFound("nacionalidad.specified=true");

        // Get all the perfilList where nacionalidad is null
        defaultPerfilShouldNotBeFound("nacionalidad.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByTelefonoIsEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where telefono equals to DEFAULT_TELEFONO
        defaultPerfilShouldBeFound("telefono.equals=" + DEFAULT_TELEFONO);

        // Get all the perfilList where telefono equals to UPDATED_TELEFONO
        defaultPerfilShouldNotBeFound("telefono.equals=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllPerfilsByTelefonoIsInShouldWork() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where telefono in DEFAULT_TELEFONO or UPDATED_TELEFONO
        defaultPerfilShouldBeFound("telefono.in=" + DEFAULT_TELEFONO + "," + UPDATED_TELEFONO);

        // Get all the perfilList where telefono equals to UPDATED_TELEFONO
        defaultPerfilShouldNotBeFound("telefono.in=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllPerfilsByTelefonoIsNullOrNotNull() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where telefono is not null
        defaultPerfilShouldBeFound("telefono.specified=true");

        // Get all the perfilList where telefono is null
        defaultPerfilShouldNotBeFound("telefono.specified=false");
    }

    @Test
    @Transactional
    public void getAllPerfilsByTelefonoIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where telefono greater than or equals to DEFAULT_TELEFONO
        defaultPerfilShouldBeFound("telefono.greaterOrEqualThan=" + DEFAULT_TELEFONO);

        // Get all the perfilList where telefono greater than or equals to UPDATED_TELEFONO
        defaultPerfilShouldNotBeFound("telefono.greaterOrEqualThan=" + UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void getAllPerfilsByTelefonoIsLessThanSomething() throws Exception {
        // Initialize the database
        perfilRepository.saveAndFlush(perfil);

        // Get all the perfilList where telefono less than or equals to DEFAULT_TELEFONO
        defaultPerfilShouldNotBeFound("telefono.lessThan=" + DEFAULT_TELEFONO);

        // Get all the perfilList where telefono less than or equals to UPDATED_TELEFONO
        defaultPerfilShouldBeFound("telefono.lessThan=" + UPDATED_TELEFONO);
    }


    @Test
    @Transactional
    public void getAllPerfilsByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        User user = UserResourceIT.createEntity(em);
        em.persist(user);
        em.flush();
        perfil.setUser(user);
        perfilRepository.saveAndFlush(perfil);
        Long userId = user.getId();

        // Get all the perfilList where user equals to userId
        defaultPerfilShouldBeFound("userId.equals=" + userId);

        // Get all the perfilList where user equals to userId + 1
        defaultPerfilShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPerfilShouldBeFound(String filter) throws Exception {
        restPerfilMockMvc.perform(get("/api/perfils?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(perfil.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoIdentificacion").value(hasItem(DEFAULT_TIPO_IDENTIFICACION.toString())))
            .andExpect(jsonPath("$.[*].identificacion").value(hasItem(DEFAULT_IDENTIFICACION)))
            .andExpect(jsonPath("$.[*].edad").value(hasItem(DEFAULT_EDAD)))
            .andExpect(jsonPath("$.[*].sexo").value(hasItem(DEFAULT_SEXO.toString())))
            .andExpect(jsonPath("$.[*].direccion").value(hasItem(DEFAULT_DIRECCION)))
            .andExpect(jsonPath("$.[*].fechaNacimiento").value(hasItem(DEFAULT_FECHA_NACIMIENTO.toString())))
            .andExpect(jsonPath("$.[*].nacionalidad").value(hasItem(DEFAULT_NACIONALIDAD.toString())))
            .andExpect(jsonPath("$.[*].telefono").value(hasItem(DEFAULT_TELEFONO)));

        // Check, that the count call also returns 1
        restPerfilMockMvc.perform(get("/api/perfils/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPerfilShouldNotBeFound(String filter) throws Exception {
        restPerfilMockMvc.perform(get("/api/perfils?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPerfilMockMvc.perform(get("/api/perfils/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingPerfil() throws Exception {
        // Get the perfil
        restPerfilMockMvc.perform(get("/api/perfils/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePerfil() throws Exception {
        // Initialize the database
        perfilService.save(perfil);

        int databaseSizeBeforeUpdate = perfilRepository.findAll().size();

        // Update the perfil
        Perfil updatedPerfil = perfilRepository.findById(perfil.getId()).get();
        // Disconnect from session so that the updates on updatedPerfil are not directly saved in db
        em.detach(updatedPerfil);
        updatedPerfil
            .tipoIdentificacion(UPDATED_TIPO_IDENTIFICACION)
            .identificacion(UPDATED_IDENTIFICACION)
            .edad(UPDATED_EDAD)
            .sexo(UPDATED_SEXO)
            .direccion(UPDATED_DIRECCION)
            .fechaNacimiento(UPDATED_FECHA_NACIMIENTO)
            .nacionalidad(UPDATED_NACIONALIDAD)
            .telefono(UPDATED_TELEFONO);

        restPerfilMockMvc.perform(put("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPerfil)))
            .andExpect(status().isOk());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeUpdate);
        Perfil testPerfil = perfilList.get(perfilList.size() - 1);
        assertThat(testPerfil.getTipoIdentificacion()).isEqualTo(UPDATED_TIPO_IDENTIFICACION);
        assertThat(testPerfil.getIdentificacion()).isEqualTo(UPDATED_IDENTIFICACION);
        assertThat(testPerfil.getEdad()).isEqualTo(UPDATED_EDAD);
        assertThat(testPerfil.getSexo()).isEqualTo(UPDATED_SEXO);
        assertThat(testPerfil.getDireccion()).isEqualTo(UPDATED_DIRECCION);
        assertThat(testPerfil.getFechaNacimiento()).isEqualTo(UPDATED_FECHA_NACIMIENTO);
        assertThat(testPerfil.getNacionalidad()).isEqualTo(UPDATED_NACIONALIDAD);
        assertThat(testPerfil.getTelefono()).isEqualTo(UPDATED_TELEFONO);
    }

    @Test
    @Transactional
    public void updateNonExistingPerfil() throws Exception {
        int databaseSizeBeforeUpdate = perfilRepository.findAll().size();

        // Create the Perfil

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPerfilMockMvc.perform(put("/api/perfils")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(perfil)))
            .andExpect(status().isBadRequest());

        // Validate the Perfil in the database
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePerfil() throws Exception {
        // Initialize the database
        perfilService.save(perfil);

        int databaseSizeBeforeDelete = perfilRepository.findAll().size();

        // Delete the perfil
        restPerfilMockMvc.perform(delete("/api/perfils/{id}", perfil.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Perfil> perfilList = perfilRepository.findAll();
        assertThat(perfilList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Perfil.class);
        Perfil perfil1 = new Perfil();
        perfil1.setId(1L);
        Perfil perfil2 = new Perfil();
        perfil2.setId(perfil1.getId());
        assertThat(perfil1).isEqualTo(perfil2);
        perfil2.setId(2L);
        assertThat(perfil1).isNotEqualTo(perfil2);
        perfil1.setId(null);
        assertThat(perfil1).isNotEqualTo(perfil2);
    }
}
