package com.nexos.bancos.web.rest;

import com.nexos.bancos.Nexos20App;
import com.nexos.bancos.domain.Tipo;
import com.nexos.bancos.repository.TipoRepository;
import com.nexos.bancos.service.TipoService;
import com.nexos.bancos.web.rest.errors.ExceptionTranslator;
import com.nexos.bancos.service.dto.TipoCriteria;
import com.nexos.bancos.service.TipoQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.nexos.bancos.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link TipoResource} REST controller.
 */
@SpringBootTest(classes = Nexos20App.class)
public class TipoResourceIT {

    private static final String DEFAULT_TIPO_CUENTA = "AAAAAAAAAA";
    private static final String UPDATED_TIPO_CUENTA = "BBBBBBBBBB";

    @Autowired
    private TipoRepository tipoRepository;

    @Autowired
    private TipoService tipoService;

    @Autowired
    private TipoQueryService tipoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restTipoMockMvc;

    private Tipo tipo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipoResource tipoResource = new TipoResource(tipoService, tipoQueryService);
        this.restTipoMockMvc = MockMvcBuilders.standaloneSetup(tipoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipo createEntity(EntityManager em) {
        Tipo tipo = new Tipo()
            .tipoCuenta(DEFAULT_TIPO_CUENTA);
        return tipo;
    }

    @BeforeEach
    public void initTest() {
        tipo = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipo() throws Exception {
        int databaseSizeBeforeCreate = tipoRepository.findAll().size();

        // Create the Tipo
        restTipoMockMvc.perform(post("/api/tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipo)))
            .andExpect(status().isCreated());

        // Validate the Tipo in the database
        List<Tipo> tipoList = tipoRepository.findAll();
        assertThat(tipoList).hasSize(databaseSizeBeforeCreate + 1);
        Tipo testTipo = tipoList.get(tipoList.size() - 1);
        assertThat(testTipo.getTipoCuenta()).isEqualTo(DEFAULT_TIPO_CUENTA);
    }

    @Test
    @Transactional
    public void createTipoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipoRepository.findAll().size();

        // Create the Tipo with an existing ID
        tipo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipoMockMvc.perform(post("/api/tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipo)))
            .andExpect(status().isBadRequest());

        // Validate the Tipo in the database
        List<Tipo> tipoList = tipoRepository.findAll();
        assertThat(tipoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllTipos() throws Exception {
        // Initialize the database
        tipoRepository.saveAndFlush(tipo);

        // Get all the tipoList
        restTipoMockMvc.perform(get("/api/tipos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoCuenta").value(hasItem(DEFAULT_TIPO_CUENTA.toString())));
    }
    
    @Test
    @Transactional
    public void getTipo() throws Exception {
        // Initialize the database
        tipoRepository.saveAndFlush(tipo);

        // Get the tipo
        restTipoMockMvc.perform(get("/api/tipos/{id}", tipo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipo.getId().intValue()))
            .andExpect(jsonPath("$.tipoCuenta").value(DEFAULT_TIPO_CUENTA.toString()));
    }

    @Test
    @Transactional
    public void getAllTiposByTipoCuentaIsEqualToSomething() throws Exception {
        // Initialize the database
        tipoRepository.saveAndFlush(tipo);

        // Get all the tipoList where tipoCuenta equals to DEFAULT_TIPO_CUENTA
        defaultTipoShouldBeFound("tipoCuenta.equals=" + DEFAULT_TIPO_CUENTA);

        // Get all the tipoList where tipoCuenta equals to UPDATED_TIPO_CUENTA
        defaultTipoShouldNotBeFound("tipoCuenta.equals=" + UPDATED_TIPO_CUENTA);
    }

    @Test
    @Transactional
    public void getAllTiposByTipoCuentaIsInShouldWork() throws Exception {
        // Initialize the database
        tipoRepository.saveAndFlush(tipo);

        // Get all the tipoList where tipoCuenta in DEFAULT_TIPO_CUENTA or UPDATED_TIPO_CUENTA
        defaultTipoShouldBeFound("tipoCuenta.in=" + DEFAULT_TIPO_CUENTA + "," + UPDATED_TIPO_CUENTA);

        // Get all the tipoList where tipoCuenta equals to UPDATED_TIPO_CUENTA
        defaultTipoShouldNotBeFound("tipoCuenta.in=" + UPDATED_TIPO_CUENTA);
    }

    @Test
    @Transactional
    public void getAllTiposByTipoCuentaIsNullOrNotNull() throws Exception {
        // Initialize the database
        tipoRepository.saveAndFlush(tipo);

        // Get all the tipoList where tipoCuenta is not null
        defaultTipoShouldBeFound("tipoCuenta.specified=true");

        // Get all the tipoList where tipoCuenta is null
        defaultTipoShouldNotBeFound("tipoCuenta.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultTipoShouldBeFound(String filter) throws Exception {
        restTipoMockMvc.perform(get("/api/tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipo.getId().intValue())))
            .andExpect(jsonPath("$.[*].tipoCuenta").value(hasItem(DEFAULT_TIPO_CUENTA)));

        // Check, that the count call also returns 1
        restTipoMockMvc.perform(get("/api/tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultTipoShouldNotBeFound(String filter) throws Exception {
        restTipoMockMvc.perform(get("/api/tipos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restTipoMockMvc.perform(get("/api/tipos/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingTipo() throws Exception {
        // Get the tipo
        restTipoMockMvc.perform(get("/api/tipos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipo() throws Exception {
        // Initialize the database
        tipoService.save(tipo);

        int databaseSizeBeforeUpdate = tipoRepository.findAll().size();

        // Update the tipo
        Tipo updatedTipo = tipoRepository.findById(tipo.getId()).get();
        // Disconnect from session so that the updates on updatedTipo are not directly saved in db
        em.detach(updatedTipo);
        updatedTipo
            .tipoCuenta(UPDATED_TIPO_CUENTA);

        restTipoMockMvc.perform(put("/api/tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipo)))
            .andExpect(status().isOk());

        // Validate the Tipo in the database
        List<Tipo> tipoList = tipoRepository.findAll();
        assertThat(tipoList).hasSize(databaseSizeBeforeUpdate);
        Tipo testTipo = tipoList.get(tipoList.size() - 1);
        assertThat(testTipo.getTipoCuenta()).isEqualTo(UPDATED_TIPO_CUENTA);
    }

    @Test
    @Transactional
    public void updateNonExistingTipo() throws Exception {
        int databaseSizeBeforeUpdate = tipoRepository.findAll().size();

        // Create the Tipo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restTipoMockMvc.perform(put("/api/tipos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipo)))
            .andExpect(status().isBadRequest());

        // Validate the Tipo in the database
        List<Tipo> tipoList = tipoRepository.findAll();
        assertThat(tipoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteTipo() throws Exception {
        // Initialize the database
        tipoService.save(tipo);

        int databaseSizeBeforeDelete = tipoRepository.findAll().size();

        // Delete the tipo
        restTipoMockMvc.perform(delete("/api/tipos/{id}", tipo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Tipo> tipoList = tipoRepository.findAll();
        assertThat(tipoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipo.class);
        Tipo tipo1 = new Tipo();
        tipo1.setId(1L);
        Tipo tipo2 = new Tipo();
        tipo2.setId(tipo1.getId());
        assertThat(tipo1).isEqualTo(tipo2);
        tipo2.setId(2L);
        assertThat(tipo1).isNotEqualTo(tipo2);
        tipo1.setId(null);
        assertThat(tipo1).isNotEqualTo(tipo2);
    }
}
