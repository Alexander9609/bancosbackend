/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { Nexos20TestModule } from '../../../test.module';
import { CuentasAsociadasDeleteDialogComponent } from 'app/entities/cuentas-asociadas/cuentas-asociadas-delete-dialog.component';
import { CuentasAsociadasService } from 'app/entities/cuentas-asociadas/cuentas-asociadas.service';

describe('Component Tests', () => {
  describe('CuentasAsociadas Management Delete Component', () => {
    let comp: CuentasAsociadasDeleteDialogComponent;
    let fixture: ComponentFixture<CuentasAsociadasDeleteDialogComponent>;
    let service: CuentasAsociadasService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Nexos20TestModule],
        declarations: [CuentasAsociadasDeleteDialogComponent]
      })
        .overrideTemplate(CuentasAsociadasDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CuentasAsociadasDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CuentasAsociadasService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
