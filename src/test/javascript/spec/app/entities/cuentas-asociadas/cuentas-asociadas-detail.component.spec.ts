/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Nexos20TestModule } from '../../../test.module';
import { CuentasAsociadasDetailComponent } from 'app/entities/cuentas-asociadas/cuentas-asociadas-detail.component';
import { CuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';

describe('Component Tests', () => {
  describe('CuentasAsociadas Management Detail Component', () => {
    let comp: CuentasAsociadasDetailComponent;
    let fixture: ComponentFixture<CuentasAsociadasDetailComponent>;
    const route = ({ data: of({ cuentasAsociadas: new CuentasAsociadas(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Nexos20TestModule],
        declarations: [CuentasAsociadasDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CuentasAsociadasDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CuentasAsociadasDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.cuentasAsociadas).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
