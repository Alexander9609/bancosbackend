/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Nexos20TestModule } from '../../../test.module';
import { CuentasAsociadasUpdateComponent } from 'app/entities/cuentas-asociadas/cuentas-asociadas-update.component';
import { CuentasAsociadasService } from 'app/entities/cuentas-asociadas/cuentas-asociadas.service';
import { CuentasAsociadas } from 'app/shared/model/cuentas-asociadas.model';

describe('Component Tests', () => {
  describe('CuentasAsociadas Management Update Component', () => {
    let comp: CuentasAsociadasUpdateComponent;
    let fixture: ComponentFixture<CuentasAsociadasUpdateComponent>;
    let service: CuentasAsociadasService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Nexos20TestModule],
        declarations: [CuentasAsociadasUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(CuentasAsociadasUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CuentasAsociadasUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CuentasAsociadasService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new CuentasAsociadas(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new CuentasAsociadas();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
