/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { Nexos20TestModule } from '../../../test.module';
import { HistorialDetailComponent } from 'app/entities/historial/historial-detail.component';
import { Historial } from 'app/shared/model/historial.model';

describe('Component Tests', () => {
  describe('Historial Management Detail Component', () => {
    let comp: HistorialDetailComponent;
    let fixture: ComponentFixture<HistorialDetailComponent>;
    const route = ({ data: of({ historial: new Historial(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Nexos20TestModule],
        declarations: [HistorialDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(HistorialDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HistorialDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.historial).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
