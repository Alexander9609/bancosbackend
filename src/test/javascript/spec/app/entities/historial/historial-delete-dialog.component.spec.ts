/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { Nexos20TestModule } from '../../../test.module';
import { HistorialDeleteDialogComponent } from 'app/entities/historial/historial-delete-dialog.component';
import { HistorialService } from 'app/entities/historial/historial.service';

describe('Component Tests', () => {
  describe('Historial Management Delete Component', () => {
    let comp: HistorialDeleteDialogComponent;
    let fixture: ComponentFixture<HistorialDeleteDialogComponent>;
    let service: HistorialService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Nexos20TestModule],
        declarations: [HistorialDeleteDialogComponent]
      })
        .overrideTemplate(HistorialDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(HistorialDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HistorialService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
