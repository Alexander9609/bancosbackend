/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { Nexos20TestModule } from '../../../test.module';
import { HistorialUpdateComponent } from 'app/entities/historial/historial-update.component';
import { HistorialService } from 'app/entities/historial/historial.service';
import { Historial } from 'app/shared/model/historial.model';

describe('Component Tests', () => {
  describe('Historial Management Update Component', () => {
    let comp: HistorialUpdateComponent;
    let fixture: ComponentFixture<HistorialUpdateComponent>;
    let service: HistorialService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Nexos20TestModule],
        declarations: [HistorialUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(HistorialUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(HistorialUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(HistorialService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Historial(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Historial();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
