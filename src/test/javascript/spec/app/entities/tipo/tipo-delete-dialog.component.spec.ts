/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { Nexos20TestModule } from '../../../test.module';
import { TipoDeleteDialogComponent } from 'app/entities/tipo/tipo-delete-dialog.component';
import { TipoService } from 'app/entities/tipo/tipo.service';

describe('Component Tests', () => {
  describe('Tipo Management Delete Component', () => {
    let comp: TipoDeleteDialogComponent;
    let fixture: ComponentFixture<TipoDeleteDialogComponent>;
    let service: TipoService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [Nexos20TestModule],
        declarations: [TipoDeleteDialogComponent]
      })
        .overrideTemplate(TipoDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(TipoDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(TipoService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
